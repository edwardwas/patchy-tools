{compiler ? "default"}:
let
  hostPkgs = import <nixpkgs> {};
  pinnedVersion = hostPkgs.lib.importJSON ./nix/nixpkgs-version.json;
    pinnedPkgs = hostPkgs.fetchFromGitHub {
      owner = "NixOs";
      repo = "nixpkgs-channels";
      inherit (pinnedVersion) rev sha256;

    };
  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self : super : rec {
          tools = hLib.dontHaddock (self.callPackage ./nix/tools.nix {});
          videoMaker = self.callPackage ./nix/videoMaker.nix {};
          combineRuns = self.callPackage ./nix/combineRuns.nix {};
          entropy = self.callPackage ./nix/entropy.nix {};
          makePlotting = self.callPackage ./nix/makePlotting.nix {};

          streaming-commons = self.callPackage ./nix/streaming-commons.nix {};
          megaparsec = self.callPackage ./nix/megaparsec.nix {};

          lens = self.callPackage ./nix/lens.nix{};

          ghc = super.ghc // {withPackages = super.ghc.withHoogle;};
          ghcWithPackages = self.ghc.withPackages;
        };
      };
    };
  };

  pkgs = import pinnedPkgs { inherit config; };
  hLib = pkgs.haskell.lib;

in
  pkgs
