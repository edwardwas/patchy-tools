{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections    #-}
{-# LANGUAGE TypeFamilies     #-}

module Tools.Graph where

import           Tools.P
import           Tools.Stats
import           Tools.Types
import           Tools.Util           (workStealer)

import           Data.Bifunctor
import qualified Data.ByteString.Lazy as B
import           Data.Csv
import           Data.List
import qualified Data.Map.Strict      as M
import           Data.Maybe           (fromJust)
import           Data.Monoid
import           Pipes
import qualified Pipes.Prelude        as P
import           Pipes.Safe
import           System.IO

data AxisFunc x y =
      AgainstTime (Int -> x)                   (ParticleType -> Frame -> y)
    | LastFrame   (ParticleType -> Frame -> x) (ParticleType -> Frame -> y)
    | FirstFrame  (ParticleType -> Frame -> x) (ParticleType -> Frame -> y)

instance Functor (AxisFunc x) where
    fmap f (AgainstTime xFunc yFunc) = AgainstTime xFunc (\pt -> f . yFunc pt)
    fmap f (LastFrame   xFunc yFunc) = LastFrame   xFunc (\pt -> f . yFunc pt)
    fmap f (FirstFrame  xFunc yFunc) = FirstFrame  xFunc (\pt -> f . yFunc pt)

instance Bifunctor AxisFunc where
    second = fmap
    first f (AgainstTime xFunc yFunc) = AgainstTime (f . xFunc) yFunc
    first f (LastFrame   xFunc yFunc) = LastFrame   (\pt -> f . xFunc pt) yFunc
    first f (FirstFrame  xFunc yFunc) = FirstFrame  (\pt -> f . xFunc pt) yFunc

loadAxisDataHelper :: (Monad m, Ord x) => AxisFunc x y -> Producer (ParticleType,Frame) m () -> m (M.Map x y)
loadAxisDataHelper (AgainstTime xFunc yFunc) prod = P.fold M.union M.empty id $
    prod >-> indexPipe >->  P.map (\(i,(pt,frame)) -> M.singleton (xFunc i) $ yFunc pt frame)
loadAxisDataHelper (LastFrame xFunc yFunc) prod = do
    (pt,frame) <- fromJust <$> P.last prod
    return $ M.singleton (xFunc pt frame) (yFunc pt frame)
loadAxisDataHelper (FirstFrame xFunc yFunc) prod = do
    (pt,frame) <- fromJust <$> P.head prod
    return $ M.singleton (xFunc pt frame) (yFunc pt frame)

dataProducer :: MonadSafe m => FilePath -> Producer (ParticleType, Frame) m ()
dataProducer fp = do
    pt <- loadParticleType fp
    frameProducer fp >-> P.map (pt,)

loadAxisData :: (ParIO m, Foldable f, Ord x, Monoid y) =>
    Int -> AxisFunc x y -> f FilePath -> m (M.Map x y)
loadAxisData n axis = P.fold (M.unionWith mappend) M.empty id .
    workStealer n (runSafeT . loadAxisDataHelper axis . dataProducer) . each

writeAxisDataCsv :: (ParIO m, ToField x, ToField y, Ord x, Foldable f, Monoid y) =>
    Int -> AxisFunc x y -> FilePath -> f FilePath -> m ()
writeAxisDataCsv n axis outFile inFile = loadAxisData n axis inFile
    >>= liftIO . B.writeFile outFile . encode . sortOn fst . M.toList

writeAxisErrorCSV :: (ParIO m, Ord x, Foldable f, ToField x , ToField y, Floating y) =>
    Int -> AxisFunc x y -> FilePath -> f FilePath -> m ()
writeAxisErrorCSV n axis outFile inFile = loadAxisData n (singleElement <$> axis) inFile
    >>= liftIO . B.writeFile outFile . encode . map (\(a,(b,c,d)) -> (a,b,c,d))
        . sortOn fst . M.toList . fmap errorTriple
