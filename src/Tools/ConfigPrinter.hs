{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}

module Tools.ConfigPrinter where

import           Tools.Types

import           Control.Lens
import           Data.Monoid
import qualified Data.Text    as T
import           Linear

data ConfigGetter s = forall a. Show a =>
                                ConfigGetter
  { name   :: T.Text
  , getter :: Getter s a
  }

particleTypeConfigGetters :: [ConfigGetter ParticleType]
particleTypeConfigGetters =
  [ ConfigGetter "theta" $ singleTheta
  , ConfigGetter "boxSizeX" boxSizeX
  , ConfigGetter "boxSizeY" boxSizeY
  , ConfigGetter "sameEnergy" sameEnergy
  , ConfigGetter "differentEnergy" differentEnergy
  , ConfigGetter "patchDiameter" patchDiameter
  , ConfigGetter "particleDiameter" particleDiameter
  , ConfigGetter "numParts" numParts
  , ConfigGetter "fileName" fileName
  , ConfigGetter "writeRate" writeRate
  , ConfigGetter "numSweeps" numSweeps
  , ConfigGetter "singleMoveMonteCarlo" singleMoveMonteCarlo
  ]

makeSingleLine :: a -> ConfigGetter a -> T.Text
makeSingleLine pt ConfigGetter {..} =
  name <> " = " <> T.pack (show $ view getter pt)

makeSingleConfig :: a -> [ConfigGetter a] -> T.Text
makeSingleConfig pt = T.intercalate "\n" . map (makeSingleLine pt)

makeAllConfig :: [a] -> [ConfigGetter a] -> T.Text
makeAllConfig pts confs =
  T.intercalate "\nNEWSIM\n" $ map (`makeSingleConfig` confs) pts

printParticle :: Particle -> T.Text
printParticle p =
  T.pack $ "PARTICLE " <> show (p ^. pos . _x) <> " , " <>
  show (p ^. pos . _y) <>
  " , " <>
  show (p ^. orientation . angleIso) <>
  "\n"

makeConfigAndParticles ::
     [ConfigGetter a] -> a -> [Particle] -> T.Text
makeConfigAndParticles gs pt parts =
  makeSingleConfig pt gs <> "\n" <>
  mconcat (map printParticle parts)

makeAllConfigAndParticles ::
     [ConfigGetter a] -> [(a, [Particle])] -> T.Text
makeAllConfigAndParticles gs =
  T.intercalate "\nNEWSIM\n" .
  map (uncurry $ makeConfigAndParticles gs)
