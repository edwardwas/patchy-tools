{-# LANGUAGE ApplicativeDo              #-}
{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TupleSections              #-}
{-# LANGUAGE UndecidableInstances       #-}

module Tools.Types where

import           Control.Applicative
import           Control.Concurrent.Async.Lifted.Safe
import           Control.Lens                         hiding (each, (.=))
import           Control.Monad
import           Control.Monad.Trans.Control
import           Data.Aeson
import           Data.Aeson.TH
import qualified Data.ByteString                      as B
import           Data.Colour                          (Colour)
import           Data.Colour.Names                    (blue, red)
import           Data.Csv                             hiding ((.:), (.=))
import           Data.Hashable
import           Data.Int
import qualified Data.IntMap                          as I
import           Data.Maybe
import           Data.SafeCopy
import           Data.Serialize
import           Data.Serialize.Text                  ()
import qualified Data.Text                            as T
import qualified Data.Text.Encoding                   as T
import           Foreign.C.Types
import           GHC.Generics                         hiding (to)
import           Linear
import           Linear.Affine
import           Pipes
import qualified Pipes.Prelude                        as P
import           Pipes.Safe
import           Test.QuickCheck
import           Test.QuickCheck.Arbitrary.Generic

instance ToField Bool where
  toField True  = "T"
  toField False = "F"

instance Arbitrary T.Text where
  arbitrary = T.pack <$> arbitrary
  shrink = map T.pack . shrink . T.unpack

instance Arbitrary a =>
         Arbitrary (V2 a) where
  arbitrary = V2 <$> arbitrary <*> arbitrary
  shrink (V2 x y) =
    concat $ do
      xs <- shrink x
      ys <- shrink y
      return [V2 x ys, V2 xs y, V2 xs ys]

instance SafeCopy a =>
         SafeCopy (V2 a) where
  version = 0
  kind = base
  putCopy p =
    contain $ do
      put_x <- getSafePut
      put_y <- getSafePut
      put_x $ p ^. _x
      put_y $ p ^. _y
      return ()
  getCopy =
    contain $ do
      get_x <- getSafeGet
      get_y <- getSafeGet
      (\x y -> V2 x y) <$> get_x <*> get_y

instance SafeCopy a =>
         SafeCopy (Point V2 a) where
  version = 0
  kind = base
  putCopy p =
    contain $ do
      put_x <- getSafePut
      put_y <- getSafePut
      put_x $ p ^. _x
      put_y $ p ^. _y
      return ()
  getCopy =
    contain $ do
      get_x <- getSafeGet
      get_y <- getSafeGet
      (\x y -> P $ V2 x y) <$> get_x <*> get_y

-- | Using async-lifted requires a collection of constraints. This type collects them all so
-- I don't have to type them all out each time.
type ParIO m = (MonadIO m, MonadBaseControl IO m, Forall (Pure m), MonadCatch m, MonadMask m)

-- | Often we need to compare floating point results. This checks if the difference between the two values
-- is 'nearZero' as defined by 'Epsilon'
--
-- prop> \(a :: Double) -> a ~= a
(~=)
  :: (Epsilon a)
  => a -> a -> Bool
(~=) a b = nearZero $ abs (a - b)

infixr 4 ~=

-- | Often we need to use values that are always unit vectors. 'UnitVector' should be considered unsafe because
-- it breaks this: use 'unitVector' or 'mkUnitVector' instead.
newtype UnitVector a = UnitVector
  { _UVec :: V2 a
  } deriving ( Eq
             , Show
             , Read
             , Functor
             , Fractional
             , Num
             , Ord
             , Metric
             , Additive
             , Floating
             , Epsilon
             , Generic
             )

makeLenses ''UnitVector

deriveSafeCopy 0 'base ''UnitVector

instance Hashable a =>
         Hashable (UnitVector a)

instance Serialize a =>
         Serialize (UnitVector a)

instance R1 UnitVector where
  _x = uVec . _x

instance R2 UnitVector where
  _y = uVec . _y
  _xy = uVec

instance (Floating a, Arbitrary a, Ord a, Epsilon a) =>
         Arbitrary (UnitVector a) where
  arbitrary =
    (\a -> UnitVector (V2 a $ 1 - a * a)) <$>
    suchThat arbitrary (\x -> x > 0 && x <= 1)
  shrink (UnitVector (V2 x y)) =
    concat $ do
      nx <- shrink x
      ny <- shrink y
      return $ [unitVector nx y, unitVector x ny, unitVector nx ny]

-- | Create a 'UnitVector' from two numbers. They will be normalized, so this is safe
--
unitVector
  :: (Epsilon a, Floating a, Fractional a, Eq a, Num a, Ord a)
  => a -> a -> UnitVector a
unitVector x y = UnitVector $ normalize $ V2 x y

-- | Normalize a standoard vector and create a unit vector
mkUnitVector
  :: (Epsilon a, Floating a)
  => V2 a -> UnitVector a
mkUnitVector = UnitVector . normalize

-- | Convert safely between a standard vector and a unit vector.
--
-- Note: This is not /technically/ a law abiding lens. @unitVectorIso . re unitVectorIso@ does not nessacerly
-- equal @id@ 'unitVectorIso' normalises the vector. Going the other way is law abiding:
-- @re unitVectorIso . unitVectorIso === id@
--
-- prop> (a :: UnitVector Double)^.angleIso ~= view (re unitVectorIso . unitVectorIso . angleIso) a
unitVectorIso
  :: (Epsilon a, Floating a)
  => Iso' (V2 a) (UnitVector a)
unitVectorIso = iso mkUnitVector _UVec

-- | Convert between a unit vector and an angle in radians.
--
angleIso
  :: (Epsilon a, RealFloat a)
  => Iso' (UnitVector a) a
angleIso =
  iso
    (\p -> atan2 (p ^. _y) (p ^. _x))
    (\t -> unitVector (cos t) (sin t))

-- | Add an angle to a unit vector. There may or may not be a more efficient way of running it.
--
-- prop> view angleIso (a :: UnitVector Double) ~= view angleIso (addAngle 0 a)
-- prop> 1 ~= norm (addAngle (a :: Double) (b :: UnitVector Double))
addAngle
  :: (Epsilon a, RealFloat a, Num a)
  => a -> UnitVector a -> UnitVector a
addAngle a = view (re angleIso) . (a +) . view angleIso

-- | Change between 'T.Text' and 'B.ByteString'. Useful for making different libraries play nicely
isoText :: Iso' T.Text B.ByteString
isoText = iso T.encodeUtf8 T.decodeUtf8

-- | This is the /colour/ of the patch, which should not be confused with a real colour.
data PatchColour
  = Red
  | Blue
  deriving (Eq, Show, Read, Enum, Bounded, Ord, Generic)

makePrisms ''PatchColour

deriveSafeCopy 0 'base ''PatchColour

instance Hashable PatchColour

instance Serialize PatchColour

instance Arbitrary PatchColour where
  arbitrary = arbitraryBoundedEnum

-- | A particle as stored in the system. It has a position, an orientation and a number of patches, each with a given
-- colour. Shared properties on the particles, such as radius and bond angle, are stored in the 'ParticleType' record.
-- This is how it is stored in the simulation at the moment, but may be changed.
data Particle = Particle
  { _particlePos         :: Point V2 Double
  , _particleOrientation :: UnitVector Double
  , _particleColours     :: [PatchColour]
  } deriving (Eq, Show, Ord, Generic)

makeFields ''Particle

makeLenses ''Particle

deriveSafeCopy 0 'base ''Particle

instance Hashable Particle

instance Serialize Particle

instance Arbitrary Particle where
  arbitrary =
    Particle <$> (P <$> arbitrary) <*> arbitrary <*>
    replicateM 4 arbitrary

instance ToJSON Particle where
  toJSON p =
    object
      [ "x" .= (p ^. pos . _x)
      , "y" .= (p ^. pos . _y)
      , "rotation" .= (p ^. orientation . angleIso)
      ]

instance FromJSON Particle where
  parseJSON (Object b) = do
    pos <- (\x y -> P $ V2 x y) <$> b .: "x" <*> b .: "y"
    rot <- (review angleIso) <$> b .: "rotation"
    return $ Particle pos rot [Red, Blue, Red, Blue]

-- | A frame corresponds to one snapshot of the simulation
type Frame = I.IntMap Particle

addParticleToFrame :: Frame -> Particle -> Frame
addParticleToFrame fr pt = fr & at k .~ Just pt
  where
    k = fromMaybe 0 $ maximumOf (itraversed . asIndex . to succ) fr

newtype ThetaHolder =
  ThetaHolder Double
  deriving (Eq, Show, Ord, Generic)

instance Hashable ThetaHolder

instance Serialize ThetaHolder

instance Arbitrary ThetaHolder where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance ToJSON ThetaHolder where
  toJSON = toJSON . allThetas

instance FromJSON ThetaHolder where
  parseJSON o = do
    (x:_:_:_) <- parseJSON o
    return $ ThetaHolder x

instance ToField ThetaHolder where
  toField (ThetaHolder a) = toField a

allThetas :: ThetaHolder -> [Double]
allThetas (ThetaHolder t) = [t, pi - t, pi + t, 2 * pi - t]

thetaHead :: Iso' ThetaHolder Double
thetaHead = iso (\(ThetaHolder t) -> t) ThetaHolder

deriveSafeCopy 0 'base ''ThetaHolder

data SimulationInfo = SimulationInfo
  { _thetas               :: ThetaHolder
  , _boxSizeX             :: Double
  , _boxSizeY             :: Double
  , _sameEnergy           :: Double
  , _differentEnergy      :: Double
  , _patchDiameter        :: Double
  , _particleDiameter     :: Double
  , _numParts             :: Integer
  , _writeRate            :: Integer
  , _numSweeps            :: Integer
  , _cellSize             :: Double
  , _rotationalDelta      :: Double
  , _spatialDelta         :: Double
  , _singleMoveMonteCarlo :: Bool
  , _exponentialSampling  :: Bool
  , _fileName             :: T.Text
  } deriving (Eq, Show, Ord, Generic)

makeClassy ''SimulationInfo

boxSize :: HasSimulationInfo x => Traversal' x Double
boxSize func si =
    (\x y -> si & boxSizeX .~ x & boxSizeY .~ y) <$> func (si ^. boxSizeX) <*>
    func (si ^. boxSizeY)

instance Hashable SimulationInfo

instance Serialize SimulationInfo

instance Arbitrary SimulationInfo where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance ToNamedRecord SimulationInfo

deriveSafeCopy 0 'base ''SimulationInfo

instance FromJSON SimulationInfo where
  parseJSON (Object b) =
    SimulationInfo <$>
    (b .: "thetas") <*>
    (b .: "boxSizeX" <|> b .: "boxSize") <*>
    (b .: "boxSizeY" <|> b .: "boxSize") <*>
    (b .: "sameEnergy") <*>
    (b .: "differentEnergy") <*>
    (b .: "patchDiameter") <*>
    (b .: "particleDiameter") <*>
    (b .: "numParts") <*>
    (b .: "writeRate") <*>
    (b .: "numSweeps") <*>
    (b .: "cellSize") <*>
    (b .: "rotationalDelta") <*>
    (b .: "spatialDelta") <*>
    (b .: "singleMoveMonteCarlo") <*>
    (b .: "exponentialSampling") <*>
    (b .: "fileName")
  parseJSON _ = mzero

singleTheta
  :: HasSimulationInfo s
  => Lens' s Double
singleTheta = thetas . thetaHead

-- | The particle type contains information for the simulation and all the particles.
data ParticleType = ParticleType
  { _particlesToAdd  :: [Particle]
  , _pinnedParticles :: [Particle]
  , _simulationSeed  :: Maybe Int
  , _particleInfo    :: SimulationInfo
  } deriving (Eq, Show, Ord, Generic)

makeClassy ''ParticleType

deriveSafeCopy 0 'base ''ParticleType

instance Hashable ParticleType

instance Serialize ParticleType

instance Arbitrary ParticleType where
  arbitrary = genericArbitrary
  shrink = genericShrink

instance HasSimulationInfo ParticleType where
  simulationInfo = particleInfo

instance ToJSON ParticleType where
  toJSON pt =
    object
      [ "thetas" .= view thetas pt
      , "boxSizeX" .= view boxSizeX pt
      , "boxSizeY" .= view boxSizeY pt
      , "sameEnergy" .= view sameEnergy pt
      , "differentEnergy" .= view differentEnergy pt
      , "patchDiameter" .= view patchDiameter pt
      , "particleDiameter" .= view particleDiameter pt
      , "numParts" .= view numParts pt
      , "fileName" .= view fileName pt
      , "writeRate" .= view writeRate pt
      , "numSweeps" .= view numSweeps pt
      , "cellSize" .= view cellSize pt
      , "rotationalDelta" .= view rotationalDelta pt
      , "spatialDelta" .= view spatialDelta pt
      , "singleMoveMonteCarlo" .= view singleMoveMonteCarlo pt
      , "particlesToAdd" .= view particlesToAdd pt
      , "pinnedParticles" .= view pinnedParticles pt
      , "exponentialSampling" .= view exponentialSampling pt
      , "seed" .=
        fromMaybe
          Null
          (Number . fromIntegral <$> view simulationSeed pt)
      ]

--
instance FromJSON ParticleType where
  parseJSON (Object b) =
    ParticleType <$>
        (b .: "particlesToAdd") <*>
        (b .: "pinnedParticles") <*>
        ((Just <$> (b .: "seed")) <|> pure Nothing) <*>
        parseJSON (Object b)
  parseJSON _ = mzero

-- | A lens into the area fraction of a particle type. It changes the boxSize to fit the
-- new area fraction
areaFraction :: Lens' ParticleType Double
areaFraction = lens getter setter
  where
    getter pt =
        0.25 * pi * np pt * (view particleDiameter pt / view boxSizeX pt) ** 2
    setter pt a =
        let n = sqrt (np pt * pi / a) * 0.5 * view particleDiameter pt
        in pt & boxSizeX .~ n & boxSizeY .~ n
    np pt =
        fromIntegral $
        fromIntegral (view numParts pt) + length (view particlesToAdd pt)

--
--
-- | A default particle type
defaultParticleType :: ParticleType
defaultParticleType =
  ParticleType
  { _particlesToAdd = []
  , _pinnedParticles = []
  , _simulationSeed = Nothing
  , _particleInfo =
      SimulationInfo
      { _thetas = ThetaHolder 0.6
      , _boxSizeX = 10
      , _boxSizeY = 10
      , _sameEnergy = 3
      , _differentEnergy = 3
      , _patchDiameter = 0.15
      , _particleDiameter = 1
      , _numParts = 30
      , _writeRate = 10
      , _numSweeps = 100
      , _cellSize = 2
      , _rotationalDelta = 0.05
      , _spatialDelta = 0.05
      , _singleMoveMonteCarlo = False
      , _exponentialSampling = False
      , _fileName = ""
      }
  }

--
-- | A prism onto the colour of patches for drawing
patchCol
  :: (Ord a, Floating a)
  => Prism' (Colour a) PatchColour
patchCol = prism' patchToCol colToPatch
  where
    colToPatch a
      | a == red = Just Red
      | a == blue = Just Blue
      | otherwise = Nothing
    patchToCol Red  = red
    patchToCol Blue = blue

-- | A pipe that adds an idexing function. It shouldn't really be here, but has no other home....
--
-- prop> \(xs :: [Double]) -> xs == map snd (P.toList $ each xs >-> indexPipe)
indexPipe
  :: Monad m
  => Pipe a (Int, a) m x
indexPipe = helper 0
  where
    helper n = await >>= yield . (n, ) >> helper (n + 1)
