{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE FlexibleContexts #-}

module Tools.Analyse where

import Tools.Analyse.HList
import Tools.P
import Tools.Physical
import Tools.Stats
import Tools.Types
import Tools.Util

import Control.Lens hiding (each, (.=))
import Control.Monad.Identity
import Control.Monad.Reader
import qualified Data.ByteString.Lazy as B
import Data.Csv
import qualified Data.HashMap.Lazy as H
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import GHC.Exts
import Pipes
import qualified Pipes.Prelude as P
import Pipes.Safe

analyseFile
  :: (Ord k, Monoid a, MonadCatch m, MonadMask m, MonadIO m)
  => (Int -> PhysicalQueryT (SafeT m) (k, a))
  -> FilePath
  -> m (M.Map k a)
analyseFile func fp =
  runSafeT $ do
    liftIO $ putStrLn ("Started on file " ++ fp)
    pt <- loadParticleType fp
    l <- fromIntegral <$> P.length (frameProducer fp)
    P.fold (M.unionWith mappend) M.empty id $
      frameProducer fp >-> indexPipe >->
      P.mapM
        (\(n, fr) ->
           (n, ) . uncurry M.singleton <$>
           runReaderT
             (runPhysicalQueryT (func n))
             (makePhysicalInfoHolder pt fr)) >->
      P.chain
        (\(n, _) ->
           liftIO $
           putStrLn
             ("Done frame " ++
              show n ++
              " for file " ++
              fp ++ ". " ++ show (100 * fromIntegral n / l) ++ "%")) >->
      P.map snd

analyseManyFiles
  :: ( ParIO m
     , Foldable f
     , ToRecord (HList Identity (Append xs ys))
     , AllConstraint Fractional ys
     , SumStat s
     , Ord (HList Identity xs)
     , Monoid (HList s ys)
     )
  => Int
  -> (Int -> PhysicalQueryT (SafeT m) (HList Identity xs, HList s ys))
  -> FilePath
  -> f FilePath
  -> m ()
analyseManyFiles n func outFile inputs =
  P.fold
    (M.unionWith mappend)
    M.empty
    id
    (workStealer n (analyseFile func) (each inputs)) >>=
  liftIO .
  B.writeFile outFile .
  encode .
  map (\(a, b) -> combineHList a (extractValueHList b)) . M.toList

-----
data AnalysisField
  = FrameNumber
  | BondStrength
  | BondAngle
  | LocalPhi
  | GlobalPhi
  | LocalPsi
  | GlobalPsi
  | Energy
  deriving (Eq, Show, Ord)

calculateAnalysisField
  :: (MonadPhysicalQuery m)
  => AnalysisField -> Int -> m Double
calculateAnalysisField FrameNumber n = return $ fromIntegral n
calculateAnalysisField BondStrength _ =
  viewParticleType (sameEnergy . to abs)
calculateAnalysisField BondAngle _ = viewParticleType singleTheta
calculateAnalysisField LocalPhi _ = runPhysicalFold localPhi6Query
calculateAnalysisField GlobalPhi _ = runPhysicalFold globalPhi6Query
calculateAnalysisField LocalPsi _ = runPhysicalFold localPsiQuery
calculateAnalysisField GlobalPsi _ = runPhysicalFold globalPsiQuery
calculateAnalysisField Energy _ = runPhysicalFold frameEnergy

analysisFieldName :: AnalysisField
                  -> RunningVariance Double
                  -> NamedRecord
analysisFieldName FrameNumber x =
  ["Frame Number" .= extractValue (meanFromVariance x)]
analysisFieldName BondStrength x =
  ["Bond Strength" .= extractValue (meanFromVariance x)]
analysisFieldName BondAngle x =
  ["Bond Angle" .= extractValue (meanFromVariance x)]
analysisFieldName LocalPhi x =
  [ "Local Phi Mean" .= extractValue (meanFromVariance x)
  , "Local Phi Error" .= standardError x
  ]
analysisFieldName GlobalPhi x =
  [ "Global Phi Mean" .= extractValue (meanFromVariance x)
  , "Global Phi Error" .= standardError x
  ]
analysisFieldName LocalPsi x =
  [ "Local Psi Mean" .= extractValue (meanFromVariance x)
  , "Local Psi Error" .= standardError x
  ]
analysisFieldName GlobalPsi x =
  [ "Global Psi Mean" .= extractValue (meanFromVariance x)
  , "Global Psi Error" .= standardError x
  ]
analysisFieldName Energy x =
  [ "Energy Mean" .= extractValue (meanFromVariance x)
  , "Energy Error" .= standardError x
  ]

analysisFieldHeder :: AnalysisField -> Header
analysisFieldHeder f =
  V.fromList $ H.keys $ analysisFieldName f (singleElement 0)

testFile
  :: (SumStat s, MonadSafe m, Show (s Double), MonadIO m)
  => [AnalysisField]
  -> [AnalysisField]
  -> FilePath
  -> m (M.Map [Double] [s Double])
testFile key field fp =
  let statusHelper
        :: MonadIO m
        => Int -> m ()
      statusHelper n =
        liftIO $
        putStrLn ("Doing frame " ++ show n ++ " for file " ++ fp)
  in do pt <- loadParticleType fp
        let helper !n !fr = M.singleton k a
              where
                !k =
                  map
                    (\f -> (calculateAnalysisField f n) (pt, fr))
                    key
                !a =
                  map
                    (\f ->
                       singleElement $
                       (calculateAnalysisField f n) (pt, fr))
                    field
        P.fold (M.unionWith mappend) M.empty id $
          frameProducer fp >-> indexPipe >->
          P.chain (statusHelper . fst) >->
          P.seq >->
          P.map (uncurry helper) >->
          P.chain (liftIO . print) >->
          P.seq

testAllFiles
  :: Foldable f
  => Int
  -> [AnalysisField]
  -> [AnalysisField]
  -> FilePath
  -> f FilePath
  -> IO ()
testAllFiles cores key field outFile fps =
  P.fold
    (M.unionWith mappend)
    M.empty
    id
    (workStealer cores (runSafeT . testFile key field) (each fps)) >>=
  B.writeFile outFile .
  encodeByName (foldMap analysisFieldHeder (key ++ field)) .
  map
    (\(a, b) ->
       mconcat $
       zipWith
         analysisFieldName
         (key ++ field)
         (map singleElement a ++ b)) .
  M.toList
