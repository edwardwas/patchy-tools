{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}

module Tools.Draw where

import           Tools.Physical
import           Tools.Stats
import           Tools.Types

import           Control.Lens                hiding (view)
import qualified Control.Lens                as L
import           Control.Monad
import           Control.Monad.Reader.Class
import           Data.Bifunctor
import           Data.Foldable
import qualified Data.IntMap                 as I
import           Data.Maybe
import           Data.Monoid
import qualified Data.Text                   as T
import           Data.Typeable
import           Diagrams                    hiding (text)
import           Diagrams.Backend.Postscript
import qualified Diagrams.Backend.Rasterific as R
import           Diagrams.Prelude            hiding (text, view, (<>))
import           Diagrams.TwoD.Text          as DT
import           Diagrams.TwoD.Vector
import           Pipes
import qualified Pipes.Prelude               as P
import           Safe
import           System.FilePath
import           System.Random

type OwnDia a
     = ( V a ~ V2
       , N a ~ Double
       , Renderable (Path V2 Double) a
       , RealFloat (N a)
       , Typeable (N a)
       , Renderable (DT.Text Double) a
       ) =>
           Diagram a

type ColourFunc = ParticleType -> Frame -> Particle -> Colour Double

padNum :: (Num a, Show a) => Int -> a -> String
padNum tarLen a =
    fromMaybe (show a) . lastMay . takeWhile ((>) (tarLen + 1) . length) .
    iterate ('0' :) $
    show a

copyAround ::
       (RealFloat a, Typeable a, Renderable (Path V2 a) b)
    => a
    -> a
    -> QDiagram b V2 a Any
    -> QDiagram b V2 a Any
copyAround dx dy pic =
    clipped (translate (0.5 *^ V2 dx dy) $ rect dx dy) $ mconcat $ do
        x <- [-dx, 0, dx]
        y <- [-dy, 0, dy]
        guard (x == 0 || y == 0)
        return $ translate (V2 x y) pic

drawPart :: ColourFunc -> ParticleType -> Frame -> Particle -> OwnDia a
drawPart colFunc pt frame part = drawParticle part
  where
    particleRadius = 0.5 * pt ^. particleDiameter
    patchRadius = 0.5 * pt ^. patchDiameter
    centerColour = colFunc pt frame part
    drawParticle part =
        mconcat
            (reverse $ drawCore :
             zipWith drawPatch (pt ^. thetas . to allThetas) (part ^. colours)) &
        rotate (part ^. orientation . angleIso . re rad) &
        translate (part ^. pos . _Point)
    drawCore = circle particleRadius & lw veryThin & fc centerColour & lc black
    drawPatch theta col =
        (circle patchRadius <>
         lw
             thin
             (fromOffsets [V2 0 0, negate $ particleRadius *^ e (theta @@ rad)])) &
        lc (col ^. re patchCol) &
        fc (col ^. re patchCol) &
        translate (particleRadius *^ e (theta @@ rad))

drawFrameWithColour :: ColourFunc -> ParticleType -> Frame -> OwnDia a
drawFrameWithColour colFunc pt frame =
    copyAround
        (pt ^. boxSizeX)
        (pt ^. boxSizeY)
        (foldMap (drawPart colFunc pt frame) (I.elems frame <> pt ^. pinnedParticles)) <>
    (rect (pt ^. boxSizeX) (pt ^. boxSizeY) & fc white &
     translate (V2 (0.5 * pt ^. boxSizeX) (0.5 * pt ^. boxSizeY)))

drawFrame :: ParticleType -> Frame -> OwnDia a
drawFrame = drawFrameWithColour (const $ const $ const white)

drawPhysicalFrame ::
       ( Monad m
       , V a ~ V2
       , N a ~ Double
       , Renderable (Path V2 Double) a
       , Renderable (DT.Text Double) a
       , RealFloat (N a)
       , Typeable (N a)
       , MonadPhysicalQuery m
       )
    => m (Diagram a)
drawPhysicalFrame = drawFrame <$> viewParticleType particleType <*> askParticles

drawFrameWithOrderParams :: (MonadPhysicalQuery m) => m (Diagram B)
drawFrameWithOrderParams = do
    sx <- viewParticleType boxSizeX
    sy <- viewParticleType boxSizeY
    let s = V2 sx sy
    frame <- askParticles
    pt <- viewParticleType id
    meanPhi <- mean . catMaybes . toList <$> itraverse singlePhi6Query frame
    meanPsi <- mean . catMaybes . toList <$> itraverse singlePsiQuery frame
    return $ translate (s * V2 0.5 0.3) (text $ "Phi 6: " <> show meanPhi) <>
        translate (s * V2 0.5 0.7) (text $ "Psi: " <> show meanPsi) <>
        drawFrame pt frame

drawToFile :: FilePath -> Diagram B -> IO ()
drawToFile fp =
    renderDia
        Postscript
        (PostscriptOptions (replaceExtension fp ".ps") (mkWidth 600) EPS)
