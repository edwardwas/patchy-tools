{-# LANGUAGE DefaultSignatures          #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE PartialTypeSignatures      #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TupleSections              #-}
module Tools.Physical where

import           Data.Complex
import           Tools.Stats
import           Tools.Types

import           Control.Applicative       (liftA2)
import qualified Control.Foldl             as L
import           Control.Lens
import           Control.Monad.Random
import           Control.Monad.Reader
import           Control.Monad.Trans.Maybe
import           Data.Foldable             (toList)
import           Data.Function
import qualified Data.IntMap               as I
import           Data.List
import           Data.Maybe
import           Data.Serialize
import           Data.Serialize.Text       ()
import           Debug.Trace
import           Linear                    hiding (conjugate, trace)
import           Linear.Affine

instance Serialize a => Serialize (Complex a)

data PhaseType
  = Kagome
  | Square
  | Rhombus
  | Disordered
  | Liquid
  deriving (Eq, Show, Enum, Read, Bounded, Ord)

allPairs :: [a] -> [(a, a)]
allPairs =
  concatMap (\(x:xs) -> zip (repeat x) xs) . takeWhile (not . null) .
  iterate tail

-- | A Large Number
inf :: Double
inf = 1e9

-- | A small number
epsilon :: Double
epsilon = 1e-5

class Monad m =>
      MonadReadParticleType m where
  viewParticleType :: Getter ParticleType a -> m a
  default viewParticleType :: MonadReader ParticleType m =>
    Getter ParticleType a -> m a
  viewParticleType = view

instance MonadReadParticleType ((->) ParticleType)
instance MonadReadParticleType ((->) (ParticleType, a)) where
  viewParticleType l = view (_1 . l)
instance MonadReadParticleType ((->) (a, ParticleType)) where
  viewParticleType l = view (_2 . l)

periodicDistanceSquared
  :: (R2 f, MonadReadParticleType m)
  => f Double -> f Double -> m Double
periodicDistanceSquared v1 v2 = do
  let dx = abs $ v1 ^. _x - v2 ^. _x
      dy = abs $ v1 ^. _y - v2 ^. _y
  bx <- viewParticleType boxSizeX
  by <- viewParticleType boxSizeY
  return $ (min dx $ bx - dx) ** 2 + (min dy $ by - dy) ** 2

periodicDistance
  :: (MonadReadParticleType m, R2 f)
  => f Double -> f Double -> m Double
periodicDistance a b = sqrt <$> periodicDistanceSquared a b

wrapToBox
  :: (MonadReadParticleType m, R2 t)
  => t Double -> m (t Double)
wrapToBox start = do
  let helper s a
        | a < 0 = helper s $ a + s
        | a >= s = helper s $ a - s
        | otherwise = a
  bxSzeX <- viewParticleType boxSizeX
  bxSzeY <- viewParticleType boxSizeY
  return $ start & _x %~ helper bxSzeX & _y %~ helper bxSzeY

hardCoreOverlap
  :: (MonadReadParticleType m)
  => Particle -> Particle -> m Bool
hardCoreOverlap a b = do
  partDia <- viewParticleType particleDiameter
  let maxSize = partDia - epsilon
  pdSquare <- periodicDistanceSquared (a ^. pos) (b ^. pos)
  return $ maxSize * maxSize >= pdSquare

patchLocations
  :: MonadReadParticleType m
  => Particle -> m [(PatchColour, Point V2 Double)]
patchLocations p = do
  partDia <- viewParticleType particleDiameter
  let calcX t =
        p ^. pos . _x + 0.5 * partDia *
        (p ^. orientation . _x * cos t - p ^. orientation . _y * sin t)
      calcY t =
        p ^. pos . _y + 0.5 * partDia *
        (p ^. orientation . _x * sin t + p ^. orientation . _y * cos t)
  zip (p ^. colours) . map (\t -> P $ V2 (calcX t) (calcY t)) <$>
    viewParticleType (thetas . to allThetas)

-- | Detect if two particles overlap
patchOverlap
  :: MonadReadParticleType m
  => Particle -> Particle -> m [(PatchColour, PatchColour)]
patchOverlap a b = do
  ptchsA <- patchLocations a
  ptchsB <- patchLocations b
  let guardFunc (_, pos1) (_, pos2) =
        (\a b -> a * a >= b) <$> viewParticleType patchDiameter <*>
        periodicDistanceSquared pos1 pos2
  map (over both fst) <$>
    filterM (uncurry guardFunc) ((,) <$> ptchsA <*> ptchsB)

patchInteractionEnergy
  :: MonadReadParticleType m
  => Particle -> Particle -> m Double
patchInteractionEnergy a b
  | a == b = return inf
  | otherwise = do
    let energyHelper a b =
          if a == b
            then negate <$> (viewParticleType sameEnergy)
            else negate <$> (viewParticleType differentEnergy)
    overlap <- hardCoreOverlap a b
    if overlap
      then return (-inf)
      else sum <$> (patchOverlap a b >>= mapM (uncurry energyHelper))

calculateNeighbours
  :: MonadReadParticleType m
  => Frame -> m (I.IntMap [Int])
calculateNeighbours frame = itraverse helper frame
  where
    helper iPart part =
      foldMap id <$>
      itraverse
        (\i p -> do
           energy <- patchInteractionEnergy p part
           if (energy < 0)
             then return [i]
             else return [])
        frame

class MonadReadParticleType m =>
      MonadPhysicalQuery m where
  askParticles :: m Frame
  askParticleNeighbour :: Int -> m [Particle]

data PhysicalInfoHolder = PhysicalInfoHolder
  { _physicalInfoHolderParticles    :: Frame
  , _physicalInfoHolderNeighbours   :: I.IntMap [Int]
  , _physicalInfoHolderParticleType :: ParticleType
  } deriving (Eq,Show)
makeLenses ''PhysicalInfoHolder

instance MonadReadParticleType ((->) PhysicalInfoHolder) where
  viewParticleType l = view (physicalInfoHolderParticleType . l)

makePhysicalInfoHolder :: ParticleType -> Frame -> PhysicalInfoHolder
makePhysicalInfoHolder pt fr =
  PhysicalInfoHolder
  { _physicalInfoHolderParticles = fr
  , _physicalInfoHolderNeighbours = calculateNeighbours fr pt
  , _physicalInfoHolderParticleType = pt
  }

newtype PhysicalQueryT m a = PhysicalQueryT
  { runPhysicalQueryT :: ReaderT PhysicalInfoHolder m a
  } deriving (Functor, Applicative, Monad, MonadIO, MonadRandom, MonadTrans)


physicalQueryFromFrame :: ParticleType -> Frame -> PhysicalQueryT m a -> m a
physicalQueryFromFrame pt fr (PhysicalQueryT act) =
  runReaderT act $ makePhysicalInfoHolder pt fr

type PhysicalQuery = PhysicalQueryT Identity

pureQueryFromFrame :: ParticleType -> Frame -> PhysicalQuery a -> a
pureQueryFromFrame pt frm =
  runIdentity . physicalQueryFromFrame pt frm

instance Monad m =>
         MonadReadParticleType (PhysicalQueryT m) where
  viewParticleType l =
    PhysicalQueryT $ view (physicalInfoHolderParticleType . l)

instance Monad m =>
         MonadPhysicalQuery (PhysicalQueryT m) where
  askParticles = PhysicalQueryT $ view physicalInfoHolderParticles
  askParticleNeighbour i =
    PhysicalQueryT $
    view (physicalInfoHolderNeighbours . singular (ix i)) >>=
    mapM (\newI -> view (physicalInfoHolderParticles . singular (ix newI)))

instance MonadPhysicalQuery ((->) PhysicalInfoHolder) where
  askParticles = view physicalInfoHolderParticles
  askParticleNeighbour i =
    view (physicalInfoHolderNeighbours . singular (ix i)) >>=
    mapM (\newI -> view (physicalInfoHolderParticles . singular (ix newI)))

instance MonadPhysicalQuery ((->) (ParticleType, Frame)) where
  askParticles (_, fr) = fr
  askParticleNeighbour i (pt, fr) =
    askParticleNeighbour i $ makePhysicalInfoHolder pt fr

instance MonadPhysicalQuery ((->) (Frame, ParticleType)) where
  askParticles (fr, _) = fr
  askParticleNeighbour i (fr, pt) =
    askParticleNeighbour i $ makePhysicalInfoHolder pt fr

frameEnergy
  :: (MonadPhysicalQuery m)
  => L.FoldM m (Int, Particle) Double
frameEnergy = L.FoldM helper (pure 0) return
  where
    helper accum (i, p) = do
      ns <- askParticleNeighbour i
      interactionE <- sum <$> mapM (patchInteractionEnergy p) ns
      overlapE <-
        (* 1e10) . fromIntegral . length <$> filterM (hardCoreOverlap p) ns
      return $ accum + interactionE + overlapE


-- | Get the mean number of bonds each particle has
numberOfBonds
  :: (MonadPhysicalQuery m, Monoid (s b), SumStat s, Num b)
  => L.FoldM m (Int, Particle) (s b)
numberOfBonds =
  L.FoldM
    (\accum (i, _) ->
       (mappend accum) . singleElement . fromIntegral . length <$> askParticleNeighbour i)
    (pure mempty)
    return

countFoldM
  :: (Integral c, Functor m)
  => L.FoldM m a b -> L.FoldM m a (b,c)
countFoldM (L.FoldM func start out) =
  L.FoldM
    (\(x, c) a -> (, c + 1) <$> func x a)
    ((, 1) <$> start)
    (\(x, c) -> (, c) <$> out x)

countFold :: Integral c => L.Fold a b -> L.Fold a (b, c)
countFold = L.simplify . countFoldM . L.generalize

-- | This is a simple test to see if the number of bonds and energy agree. If abs (sameEnergy) == abs (differentEnergy), the two numbers should be equal

bondsTest
  :: MonadPhysicalQuery m
  => m (Double, Double)
bondsTest = do
  (bnds, eng) <-
    runPhysicalFold
      ((,) <$> (fromMaybe 0 . extractMeanMaybe <$> numberOfBonds) <*>
       frameEnergy)
  strength <- viewParticleType sameEnergy
  numParts <- fromIntegral . length <$> askParticles
  return (bnds, eng / (strength * numParts))

particlesInteracting
  :: MonadPhysicalQuery m
  => Particle -> Particle -> m Bool
particlesInteracting a b = do
  patchDistance <- viewParticleType patchDiameter
  particleDistance <- viewParticleType particleDiameter
  if qd (a ^. pos) (b ^. pos) <= (patchDistance + particleDistance) ^
     2
    then helper <$> patchInteractionEnergy a b
    else return False
  where
    helper pie = (a /= b) && (abs pie > 0) && (abs pie < inf)


angleWithYAxis
  :: (RealFloat a)
  => UnitVector a -> a
angleWithYAxis u = atan2 (u ^. _y) (u ^. _x)

yAxisAngle
  :: (R2 f, MonadPhysicalQuery m)
  => f Double -> f Double -> m Double
yAxisAngle p1 p2 = atan2 <$> helperX <*> helperY
  where
    helperX = do
      bxSizeX <- viewParticleType boxSizeX
      return $ minimumBy (compare `on` abs) $
        map
          (\a -> p1 ^. _x - a)
          [p2 ^. _x, p2 ^. _x - bxSizeX, p2 ^. _x + bxSizeX]
    helperY = do
      bxSizeY <- viewParticleType boxSizeY
      return $ minimumBy (compare `on` abs) $
        map
          (\a -> p1 ^. _y - a)
          [p2 ^. _y, p2 ^. _y - bxSizeY, p2 ^. _y + bxSizeY]

runPhysicalFold
  :: MonadPhysicalQuery m
  => L.FoldM m (Int, Particle) a -> m a
runPhysicalFold lf = askParticles >>= L.foldM lf . I.toList

localQuery
  :: (Monad m, Fractional b)
  => (Int -> Particle -> m a) -> (a -> b) -> L.FoldM m (Int, Particle) b
localQuery q f =
  (/) <$> (L.FoldM (\acc (p, i) -> (acc +) . f <$> (q p i)) (pure 0) return) <*>
  L.generalize (L.genericLength)

globalQuery
  :: (Fractional b, MonadPhysicalQuery m)
  => (Int -> Particle -> m a) -> (a -> a -> b) -> L.FoldM m (Int, Particle) b
globalQuery q f =
  let singleValues = askParticles >>= fmap toList . itraverse q
      helper ss i p = do
        x <- q i p
        return $ sum $ map (f x) ss
  in (/) <$>
     (L.FoldM
        (\(ss, acc) (i, p) -> (\a -> (ss, acc + a)) <$> helper ss i p)
        ((, 0) <$> singleValues)
        (pure . snd)) <*>
     L.generalize L.genericLength

localPhi6Query :: (MonadPhysicalQuery m) => L.FoldM m (Int,Particle) Double
localPhi6Query = localQuery singlePhi6Query (fromMaybe 0 . fmap magnitude)

localPsiQuery :: (MonadPhysicalQuery m) => L.FoldM m (Int,Particle) Double
localPsiQuery = localQuery singlePsiQuery (fromMaybe 0)

globalPhi6Query :: MonadPhysicalQuery m => L.FoldM m (Int,Particle) Double
globalPhi6Query =
  globalQuery
    singlePhi6Query
    (\a b -> magnitude ((fromMaybe 0 a) * (fromMaybe 0 (conjugate <$> b))))

globalPsiQuery :: MonadPhysicalQuery m => L.FoldM m (Int,Particle) Double
globalPsiQuery =
  globalQuery singlePsiQuery (\a b -> fromMaybe 0 ((*) <$> a <*> b))

singlePhi6Query
  :: (MonadPhysicalQuery m)
  => Int -> Particle -> m (Maybe (Complex Double))
singlePhi6Query i particle = do
  let ang p1 p2 = yAxisAngle (p1 ^. pos) (p2 ^. pos)
  iParts <- askParticleNeighbour i
  if length iParts < 2
    then return Nothing
    else fmap Just $ mean . map (\a -> exp $ 0 :+ 6 * a) <$>
         mapM (ang particle) iParts

singlePsiQuery
  :: (MonadPhysicalQuery m)
  => Int -> Particle -> m (Maybe Double)
singlePsiQuery i particle = do
  iParts <- askParticleNeighbour i
  return $
    if length iParts < 2
      then Nothing
      else Just $
           mean
             (fmap
                (\p ->
                   (2 *
                    ((particle ^. orientation) `dot` (p ^. orientation)) ** 2) -
                   1)
                iParts)



classifyPhase :: Double -> Double -> Double -> PhaseType
classifyPhase nb phi psi
  | nb <= 2.5 = Liquid
  | phi >= 0.8 && psi <= -0.4 && psi >= -0.55 = Kagome
  | phi <= 0.45 && psi <= -0.55 = Square
  | otherwise = Disordered

classifyPhysical
  :: MonadPhysicalQuery m
  => L.FoldM m (Int, Particle) PhaseType
classifyPhysical =
  classifyPhase <$> (fromMaybe 0 . extractMeanMaybe <$> numberOfBonds) <*>
  localPhi6Query <*>
  localPsiQuery
