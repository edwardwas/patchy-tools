{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

module Tools.Util where

import Tools.Draw
import Tools.Physical
import Tools.Types

import Control.Concurrent.Async.Lifted.Safe
import Control.Lens hiding (each)
import Control.Monad
import Control.Monad.State.Strict
import Control.Monad.Trans.Control
import Data.Attoparsec.Text (skipSpace)
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Pipes
import qualified Pipes.Attoparsec as P
import Pipes.Concurrent
import qualified Pipes.Prelude as P
import Pipes.Safe
import qualified Pipes.Text.IO as PT
import System.FilePath
import System.Random
import qualified Turtle as TU

workStealer ::
     ParIO m
  => Int
  -> (a -> m b)
  -> Producer a m ()
  -> Producer b m ()
workStealer n func inProd = do
  (output, input) <- liftIO $ spawn unbounded
  (colOutput, colInput) <- liftIO $ spawn unbounded
  as <-
    lift $
    forM [0 .. n] $ \i ->
      async $ do
        runEffect $
          fromInput input >-> P.mapM func >-> toOutput colOutput
        liftIO performGC
  bs <-
    lift $
    async $ do
      runEffect $ inProd >-> toOutput output
      liftIO performGC
  lift $ mapM_ wait (bs : as)
  fromInput colInput

workStealer_ ::
     ParIO m => Int -> (a -> m ()) -> Producer a m () -> m ()
workStealer_ n func inProd = do
  (output, input) <- liftIO $ spawn unbounded
  as <-
    forM [0 .. n] $ \i ->
      async $ do
        runEffect $ fromInput input >-> P.mapM_ func
        liftIO performGC
  a <-
    async $ do
      runEffect $ inProd >-> toOutput output
      liftIO performGC
  mapM_ wait $ a : as
--drawVidWithFrameFunc :: ParIO m =>
--    (pt -> fr -> Diagram R.Rasterific) -> FilePath -> pt -> Producer fr m () -> m ()
--drawVidWithFrameFunc func outName pt framePr = do
--    prepString <- liftIO $ replicateM 5 $ randomRIO ('a','z')
--    let mkName :: Int -> String
--        mkName n = prepString ++ "img" ++ padNum 3 n ++ ".png"
--    workStealer_ 24 (\(n,d) -> liftIO $ R.renderRasterific (mkName n) (mkWidth 800) d) $
--        framePr >-> P.map (func pt) >-> indexPipe
--    TU.void $ TU.shell ("ffmpeg -framerate 2 -i " <> T.pack prepString <>
--        "img%03d.png -c:v libx264 -r 30 -pix_fmt yuv420p " <> T.pack outName)  mempty
--    TU.sh $ TU.pwd >>= TU.find (TU.prefix (TU.text $ T.pack prepString) <> TU.suffix ".png") >>= TU.rm
--
--drawVid :: ParIO m => FilePath -> ParticleType -> Producer Frame m () -> m ()
--drawVid = drawVidWithFrameFunc drawFrame
