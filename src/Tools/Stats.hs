{-# LANGUAGE AllowAmbiguousTypes        #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module Tools.Stats where

import qualified Data.Map.Strict as M
import           Data.Monoid     hiding ((<>))
import           Data.Semigroup
import           GHC.Generics    (Generic)
import           Test.QuickCheck

class SumStat s where
  singleElement :: Num a => a -> s a
  extractValue
    :: Fractional a
    => s a -> a
  sumStatFromList
    :: (Foldable t, Monoid (s a), Fractional a)
    => t a -> s a
  sumStatFromList = foldMap singleElement

data RunningTotal a = RunningTotal
  { currentTotal :: !a
  , numElems     :: !Int
  } deriving (Eq, Show, Functor, Generic)

instance Semigroup a => Semigroup (RunningTotal a) where
    RunningTotal t1 n1 <> RunningTotal t2 n2 = RunningTotal (t1 <> t2) (n1 + n2)

instance (Semigroup a, Monoid a) => Monoid (RunningTotal a) where
    mempty = RunningTotal mempty 0
    mappend = (<>)

newtype RunningMean a =
  RunningMean (RunningTotal (Sum a))
  deriving (Eq, Show, Monoid, Functor,Semigroup)

extractMeanMaybe
  :: Fractional a
  => RunningMean a -> Maybe a
extractMeanMaybe (RunningMean (RunningTotal (Sum top) bot))
  | bot == 0 = Nothing
  | otherwise = Just $ top / fromIntegral bot

instance (Ord a, Fractional a) =>
         Ord (RunningMean a) where
  compare a b = compare (extractValue a) (extractValue b)

instance SumStat RunningMean where
  singleElement x = RunningMean $ RunningTotal (Sum x) 1
  extractValue (RunningMean (RunningTotal t n)) =
    getSum t / fromIntegral n

newtype RunningVariance a =
  RunningVariance (RunningTotal (Sum a, Sum a))
  deriving (Eq, Show, Monoid, Functor, Semigroup)

instance (Ord a, Fractional a) =>
         Ord (RunningVariance a) where
  compare a b = compare (extractValue a) (extractValue b)

instance SumStat RunningVariance where
  singleElement x =
    RunningVariance $ RunningTotal (Sum x, Sum $ x * x) 1
  extractValue (RunningVariance (RunningTotal (a, b) n)) =
    (getSum b / fromIntegral n) -
    (getSum a / fromIntegral n) * (getSum a / fromIntegral n)

meanFromVariance :: RunningVariance a -> RunningMean a
meanFromVariance (RunningVariance (RunningTotal (a, _) n)) =
  RunningMean $ RunningTotal a n

standardDeviation
  :: Floating a
  => RunningVariance a -> a
standardDeviation = sqrt . extractValue

standardError
  :: Floating a
  => RunningVariance a -> a
standardError v@(RunningVariance (RunningTotal _ n)) =
  standardDeviation v / fromIntegral (n - 1)

errorTriple
  :: Floating t
  => RunningVariance t -> (t, t, t)
errorTriple v =
  ( extractValue (meanFromVariance v)
  , standardError v
  , -standardError v)

mean
  :: forall a t.
     (Num a, Fractional a, Foldable t)
  => t a -> a
mean xs = extractValue (sumStatFromList xs :: RunningMean a)

safeMean
  :: (Fractional a, Foldable t)
  => t a -> Maybe a
safeMean xs
  | length xs == 0 = Nothing
  | otherwise = Just $ mean xs
