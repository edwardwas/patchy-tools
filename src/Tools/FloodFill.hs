{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}

module Tools.FloodFill
  ( findClusters
  , weightedOrderParam
  , orderParamList
  , physicalClusters
  ) where

import Tools.Physical
import Tools.Types

import Control.Lens
import Control.Monad.Reader
import Control.Monad.State
import Data.Foldable (toList)
import qualified Data.IntMap as I
import Data.List
import qualified Data.Set as S

data ClusterState a = ClusterState
  { _contents :: I.IntMap a
  , _clusters :: [[Int]]
  , _toSearch :: S.Set Int
  } deriving (Eq, Show)

makeLenses ''ClusterState

createClusterState :: [a] -> ClusterState a
createClusterState xs =
  ClusterState
  { _contents = I.fromList (zip [0 ..] xs)
  , _clusters = []
  , _toSearch = S.fromList $ zipWith (curry fst) [0 ..] xs
  }

clusterStateToList :: ClusterState a -> [[a]]
clusterStateToList c =
  map (map (\x -> (c ^. contents) I.! x) . nub) $ c ^. clusters

handleElement
  :: MonadState (ClusterState a) m
  => (a -> a -> Bool) -> Int -> m ()
handleElement touching ind = do
  inCluster <- uses toSearch $ not . S.member ind
  toSearch %= S.delete ind
  unless inCluster $ do
    toSearch <-
      use toSearch >>= filterM (applyTouching touching ind) .
      S.toAscList
    clusters . _last <>= toSearch
    mapM_ (handleElement touching) toSearch

applyTouching
  :: MonadState (ClusterState a) m
  => (a -> a -> Bool) -> Int -> Int -> m Bool
applyTouching func a b = do
  aContents <- preuse $ contents . ix a
  bContents <- preuse $ contents . ix b
  return $ Just True == (func <$> aContents <*> bContents)

runClusters
  :: MonadState (ClusterState a) m
  => (a -> a -> Bool) -> m ()
runClusters touching = do
  allClustersFound <- uses toSearch S.null
  unless allClustersFound $ do
    nextElem <- uses toSearch S.findMin
    clusters %= (++ [[nextElem]])
    handleElement touching nextElem
    runClusters touching

-- | Takes a function to check if two elements are touching and a list of elements. Returns a list of
-- lists of elements, where the elements of each list are touching, but not touching any other elements.
-- The order of elements will not be conserved
findClusters :: (a -> a -> Bool) -> [a] -> [[a]]
findClusters touching =
  clusterStateToList . execState (runClusters touching) .
  createClusterState

-- | Find all the clusters
physicalClusters
  :: MonadPhysicalQuery m
  => m [[Particle]]
physicalClusters = do
  pureQuery <-
    do partType <- viewParticleType id
       frame <- askParticles
       return $ \a ->
         pureQueryFromFrame partType frame . particlesInteracting a
  findClusters pureQuery . toList <$> askParticles

-- | Weight a order param
weightedOrderParam
  :: (Fractional a, Monad m)
  => (Particle -> PhysicalQueryT m a) -> PhysicalQueryT m a
weightedOrderParam paramFunc = do
  particleClusters <- physicalClusters
  summedValues <-
    sum <$>
    forM
      particleClusters
      (\ps ->
         sum . map (* (fromIntegral $ length ps)) <$>
         mapM paramFunc ps)
  let denom =
        fromIntegral $ sum $
        map (\x -> length x * length x) particleClusters
  return $ summedValues / denom

-- | Calculate the order parameter for each particles. Each one is repeated for the size of the cluster
orderParamList
  :: Monad m
  => (Particle -> PhysicalQueryT m a) -> PhysicalQueryT m [a]
orderParamList paramFunc = do
  particleClusters <- physicalClusters
  concat <$>
    forM
      particleClusters
      (\ps ->
         concatMap (replicate (length ps)) <$> mapM paramFunc ps)
