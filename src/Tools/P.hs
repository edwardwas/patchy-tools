{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeApplications    #-}

module Tools.P where

import           Tools.Types

import           Control.Lens               hiding (each)
import           Control.Monad              (forM, join, void)
import qualified Data.IntMap                as I
import           Data.Maybe
import           Data.Monoid                (Endo (..), appEndo, (<>))
import qualified Data.Text                  as T
import           Data.Void
import           Linear                     (V2 (..))
import           Linear.Affine              (Point (..))
import           Pipes
import           Pipes.Safe
import           System.IO
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import           Text.Read                  (readMaybe)

type Parser = Parsec Void T.Text

class ParseOption a where
  parseOptionType :: Parser a

instance ParseOption a => ParseOption [a] where
  parseOptionType =
    between (string "[") (string "]") $
    parseOptionType `sepBy` (space *> string "," <* space)

instance ParseOption a => ParseOption (Maybe a) where
  parseOptionType = (Just <$> parseOptionType) <|> pure Nothing

instance ParseOption Bool where
  parseOptionType = (> (0 :: Integer)) <$> L.decimal

instance ParseOption Integer where
  parseOptionType = L.signed space L.decimal

instance ParseOption Int where
  parseOptionType = fromInteger <$> parseOptionType @Integer

instance ParseOption ThetaHolder where
  parseOptionType = do
    (x:_:_:_) <- parseOptionType @[Double]
    return $ ThetaHolder x

instance ParseOption Double where
  parseOptionType =
    (do negFunc <- option id (negate <$ char '-')
        fullPart <- many numberChar
        decimalPart <- option "" $ (:) <$> char '.' <*> many numberChar
        exponentPart :: Integer <- option 0 $ char 'e' *> parseOptionType
        part <- case readMaybe (fullPart ++ decimalPart) of
            Just p  -> return p
            Nothing -> empty
        return $ (negFunc part) * (10 ^^ exponentPart)) <?>
    "Reading double"

instance ParseOption T.Text where
  parseOptionType =
    T.pack <$> (char '"' *> manyTill asciiChar (char '"'))

-- | Helper for parsing options
parseOption ::
     ParseOption a
  => Setter' ParticleType a
  -> T.Text
  -> Parser (Endo ParticleType)
parseOption l namStr =
  (string namStr >> space >> char '=' >> space >>
   Endo . set l <$> parseOptionType) <?>
  ("Parse option: " ++ T.unpack namStr)

allOptions :: [Parser (Endo ParticleType)]
allOptions =
  [ parseOption thetas "theta"
  , parseOption boxSize "boxSize"
  , parseOption boxSizeX "boxSizeX"
  , parseOption boxSizeY "boxSizeY"
  , parseOption sameEnergy "sameEnergy"
  , parseOption differentEnergy "differentEnergy"
  , parseOption patchDiameter "patchDiameter"
  , parseOption particleDiameter "particleDiameter"
  , parseOption numParts "numParts"
  , parseOption fileName "fileName"
  , parseOption writeRate "writeRate"
  , parseOption numSweeps "numSweeps"
  , parseOption cellSize "cellSize"
  , parseOption rotationalDelta "rotationalDelta"
  , parseOption spatialDelta "spatialDelta"
  , parseOption singleMoveMonteCarlo "singleMoveMonteCarlo"
  , parseOption exponentialSampling "exponentialSampling"
  , parseOption simulationSeed "seed"
  ]

-- | Parse a `ParticleType` with the given options. The first argument is a `ParticleType` to work from,
-- usually the `defaultParticleType`
parseParticleTypeHelper ::
     ParticleType
  -> [Parser (Endo ParticleType)]
  -> Parser ParticleType
parseParticleTypeHelper startType options = do
  _ <- char '{'
  optionEndos <-
    ((space *> choice options <* space) `manyTill` char '}') <?>
    "Parse Options"
  return $ appEndo (mconcat optionEndos) startType

--  | Parse a `ParticleType` using sensible defaults
parseParticleType :: Parser ParticleType
parseParticleType =
  parseParticleTypeHelper defaultParticleType allOptions <?>
  "Particle type"

-- | Parse the git commit version. This is stored as an `Int`, but really should probably by a string,
-- but that's harder to write
parseGitVersion :: Parser Integer
parseGitVersion =
  (option 0 $
   string "GIT VERSION: " *> L.hexadecimal <*
   option () (void $ string "-dirty")) <?>
  "Parse git version"

-- | Parse the patch colour. What did you expect
parsePatchColour :: Parser PatchColour
parsePatchColour =
  (string "Red" *> pure Red <|> string "Blue" *> pure Blue) <?>
  "Parse colour"

-- | Parse a particle. This should be a single line and does NOT skip any space
parseParticle :: Parser Particle
parseParticle =
  (do a <- parseOptionType <* space
      b <- parseOptionType <* space
      c <- parseOptionType <* space
      d <- parseOptionType <* space
      cols <- parsePatchColour `sepBy` (char ' ')
      return $ Particle (P $ V2 a b) (unitVector c d) cols) <?>
  "Particle"

--
-- | Parse a single frame. This should be fine even if there is an accepted line
parseFrame :: Parser Frame
parseFrame =
  (do n <- L.decimal <?> "Number of particles"
      eol >> space
      fr <-
        forM
          [1 .. n]
          (\n ->
             (space *> parseParticle <* space) <?>
             ("Parsing particle " ++ show n))
      space
      void (try $ string "Accepted: " *> L.decimal <* eol <* space) <|>
        return () <?> "Parsing acceptance"
      return $ I.fromList $ zip [0 ..] fr) <?>
  "Frame"

-- | Load a simulation file. This should be quite speady
--
-- The number of particles in the `ParticleType` is set from the first frame. If the number of particles changes over time this will break
loadSimulationFile ::
     MonadSafe m => FilePath -> m (ParticleType, Producer Frame m ())
loadSimulationFile fp =
  let p :: Parser (ParticleType, [Frame])
      p = do
        space
        parseGitVersion
        space
        pt <- parseParticleType
        space
        frames <- many (space *> parseFrame <* space)
        return (pt, frames)
  in bracket (liftIO $ openFile fp ReadMode) (liftIO . hClose) $ \h -> do
       contets <- liftIO $ hGetContents h
       case runParser p fp (T.pack contets) of
         Left e -> throwM e
         Right (pt, frame) ->
           return $
           (pt & numParts .~ fromIntegral (length $ head frame), each frame)

-- | Read in the frames from a file. For backwards compatability
frameProducer :: MonadSafe m => FilePath -> Producer Frame m ()
frameProducer = join . lift . fmap snd . loadSimulationFile

-- | Load a particle type. For backwards compatability
loadParticleType :: MonadSafe m => FilePath -> m ParticleType
loadParticleType = fmap fst . loadSimulationFile
