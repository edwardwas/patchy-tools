{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE RankNTypes    #-}

module Tools.Template where

import           Tools.Types

import           Control.Lens
import           Data.Monoid
import           Data.Semigroup (Semigroup)
import qualified Data.Semigroup as S
import           Data.String

newtype NameTemplate a s = NameTemplate {runNameTemplate :: a -> s}
    deriving (Functor)

instance IsString s => IsString (NameTemplate a s) where
    fromString = NameTemplate . const . fromString

instance Semigroup s => Semigroup (NameTemplate a s) where
  NameTemplate fA <> NameTemplate fB = NameTemplate $ \x -> fA x S.<> fB x

instance (Semigroup s, Monoid s) => Monoid (NameTemplate a s) where
    mempty = NameTemplate $ const mempty
    mappend = (S.<>)

fieldTemplate :: (IsString s, Show x) => Getter a x -> NameTemplate a s
fieldTemplate l = NameTemplate (fromString . show . view l)
