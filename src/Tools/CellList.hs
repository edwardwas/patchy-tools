{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Tools.CellList where

import Control.Applicative
import Control.Lens
import qualified Data.IntMap as I
import qualified Data.Map as M
import Data.Maybe
import Data.Profunctor
import Linear

data CellList a b = CellList
  { _cellListPosFunc :: a -> V2 Int
  , _cellListSize :: V2 Int
  , _cellListItems :: M.Map (V2 Int) (I.IntMap b)
  } deriving (Foldable, Functor, Traversable)

makeLenses ''CellList

type instance Index (CellList a b) = Int

type instance IxValue (CellList a b) = b

instance Ixed (CellList a b) where
  ix n = cellListItems . traverse . ix n

instance Profunctor CellList where
  rmap = fmap
  lmap func (CellList f i s) = CellList (f . func) i s

instance FunctorWithIndex (V2 Int, Int) (CellList a) where
  imap func cl =
    cl &
    over cellListItems (imap (\pos -> imap (\i -> func (pos, i))))

instance FoldableWithIndex (V2 Int, Int) (CellList a)

instance TraversableWithIndex (V2 Int, Int) (CellList a) where
  itraverse func = sequenceA . imap func

emptyCellList :: V2 Int -> (a -> V2 Int) -> CellList a a
emptyCellList sz func = CellList func sz M.empty

modPosition
  :: Integral a
  => V2 a -> V2 a -> V2 a
modPosition (V2 x y) (V2 mx my) = V2 (x `mod` mx) (y `mod` my)

addToCellList :: a -> CellList a a -> CellList a a
addToCellList a cl =
  cl & cellListItems . ix (pos `modPosition` (cl ^. cellListSize)) .
  at k .~
  Just a
  where
    k =
      fromMaybe 0 $
      maximumOf
        (cellListItems . traverse . itraversed . asIndex . to succ)
        cl
    pos = _cellListPosFunc cl a

indexPosition :: CellList a b -> Int -> Maybe (V2 Int)
indexPosition cl i =
  ifoldr
    (\(p, j) _ a ->
       a <|>
       (if i == j
          then Just p
          else Nothing))
    Nothing
    cl

cellListNeighbours :: Int -> Traversal' (CellList a b) b
cellListNeighbours n func cl =
  case indexPosition cl n of
    Just p ->
      itraverse
        (\(pos, _) b ->
           if elem pos possibleP
             then func b
             else pure b)
        cl
      where possibleP :: [V2 Int] =
              (\x y ->
                 (p & _x +~ x & _y +~ y) `modPosition`
                 (cl ^. cellListSize)) <$>
              [-1, 0, 1] <*>
              [-1, 0, 1]
