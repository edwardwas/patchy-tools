{-# OPTIONS_GHC -fno-warn-deprecations #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Tools.StartPositions where

import           Tools.Physical
import           Tools.Types

import           Control.Lens
import           Control.Monad.Error
import           Control.Monad.Random
import           Control.Monad.Reader
import qualified Data.IntMap          as I
import           Linear
import           Linear.Affine

-- | Find the center of the selection of particles. Should only be Nothing if the list is empty
centerOfParticles
  :: Traversable f
  => f Particle -> Maybe (V2 Double)
centerOfParticles parts =
  V2 <$> (mid <$> minX <*> maxX) <*> (mid <$> minY <*> maxY)
  where
    minX = minimumOf (traverse . pos . _x) parts
    maxX = maximumOf (traverse . pos . _x) parts
    minY = minimumOf (traverse . pos . _y) parts
    maxY = maximumOf (traverse . pos . _y) parts
    mid a b = (a + b) / 2

-- | Rotate a vector by an angle in radians
rotateAroundCenter
  :: Floating a
  => a -> V2 a -> V2 a
rotateAroundCenter angle vec =
  V2
    (cos angle * vec ^. _x - sin angle * vec ^. _y)
    (sin angle * vec ^. _x + cos angle * vec ^. _y)

-- | Rotate a cluster by an angle, in theory round the center
rotateCluster :: ParticleType -> Double -> [Particle] -> [Particle]
rotateCluster pt angle =
  (traverse %~ wrapPosition (pt ^. boxSizeX, pt ^. boxSizeY)) .
  (traverse . pos . _Point %~ rotateAroundCenter angle) .
  (traverse . orientation . angleIso +~ angle) .
  (traverse . pos . _x +~ (pt ^. boxSizeX)) .
  (traverse . pos . _y +~ (pt ^. boxSizeY))

-- | Move a particle inside a box
wrapPosition
  :: (Ord b, Num b, R2 f, HasPos a (f b))
  => (b,b) -> a -> a
wrapPosition (sizeX,sizeY) = (pos . _x %~ helper sizeX) . (pos . _y %~ helper sizeY)
  where
    helper size x
      | x >= size = helper size $ x - size
      | x < 0 = helper size $ x + size
      | otherwise = x

-- | Create an n by m diamond grid
diamondGrid :: ParticleType -> Int -> Int -> [Particle]
diamondGrid pt n m = do
  let dispVect =
        1.05 * pt ^. particleDiameter *^
        (pt ^. singleTheta . re angleIso . _xy)
      startPos = V2 0 $ fromIntegral m * dispVect ^. _y
      startPart =
        Particle
          (P $ (fromIntegral $ m - 1) *^ dispVect)
          (unitVector 1 0)
          [Red, Blue, Red, Blue]
  x <- [0 .. fromIntegral $ n - 1]
  y <- [0 .. fromIntegral $ m - 1]
  return $ startPart & pos . _x +~ (x + y) * dispVect ^. _x + 0.75 *
    fromIntegral (n + m - 2) &
    pos .
    _y +~
    (x - y) *
    (dispVect ^. _y) +
    pt ^.
    particleDiameter

-- | Create an n by m diamond grid
squareGrid :: ParticleType -> Int -> Int -> [Particle]
squareGrid pt n m = do
    let seperation =
            0.5 * 1.05 * pt ^. particleDiameter *
            (cos (pt ^. singleTheta) + sin (pt ^. singleTheta))
        startPart =
            Particle
                (P $
                 V2
                     (0.5 * fromIntegral n * seperation)
                     (fromIntegral m * seperation))
                (unitVector 1 0)
                [Red, Blue, Red, Blue]
    x <- [0 .. fromIntegral $ n - 1]
    y <- [0 .. fromIntegral $ m - 1]
    return $ wrapPosition (pt ^. boxSizeX, pt ^. boxSizeY) $ startPart
      & pos . _x +~ (x + y) * seperation
      & pos . _y +~ (x - y) * seperation
      & orientation . angleIso .~ if even (floor $ x + y)
                    then 0
                    else 0.5 * pi

-- | Add a new layer for the kagome lattice
addKagomeGridLayer :: ParticleType -> [Particle] -> [Particle]
addKagomeGridLayer pt = concatMap replaceSingle
  where
    kagUnitVector t =
      rotateAroundCenter t $ V2 0 $ (2 / sqrt 3) *
      (pt ^. particleDiameter - 0.5 * pt ^. patchDiameter) *
      sin (2 * pi / 3 - pt ^. singleTheta)
    replaceSingle part =
      part :
      map
        (\t ->
           part & pos +~
           P (kagUnitVector ((part ^. orientation . angleIso) - t)) &
           orientation %~
           addAngle t)
        [pi / 3, 2 * pi / 3, -pi / 3, -2 * pi / 3]

-- | Create a kagome grid with at least n particles
kagomeGrid :: ParticleType -> Int -> [Particle]
kagomeGrid pt requiredParts =
  map (wrapPosition bs) $ head $
  dropWhile ((< requiredParts) . length) $
  iterate (remover . addKagomeGridLayer pt) [startPart]
  where
    startPart =
      Particle
        (P $ V2 (0.5 * bs ^. _1) (0.5 * bs ^. _2))
        (unitVector 1 0)
        [Red, Blue, Red, Blue]
    bs = (pt ^. boxSizeX, pt ^. boxSizeY)
    remover [] = []
    remover [x] = [x]
    remover (x:xs) = x : remover (filter (not . helper x) xs)
      where
        helper a b =
          abs (a ^. pos . _x - b ^. pos . _x) < 0.1 &&
          abs (a ^. pos . _y - b ^. pos . _y) <
          0.1

-- | Make a kagome grid of three particles. This is better than 'kagomeGrid' as the particle positions do not depend
-- on the bond angle.
--
-- TODO: Replace 'kagomeGrid' with something based of of this.
kagGridSmall :: Double -> ParticleType -> Frame
kagGridSmall exp pt =
  I.fromList $ zip [0 ..] $
  map ((pos +~ pure 5) . helper) [0, 2 * pi / 3, 4 * pi / 3]
  where
    helper ang =
      Particle
        (P $ ((1 + exp) * (1 / sqrt 3) * pt ^. particleDiameter) *^
         (ang ^. re angleIso . _xy))
        (view (re angleIso) ang)
        [Red, Blue, Red, Blue]

physicalQueryFromFunction :: ParticleType
                          -> (ParticleType -> Frame)
                          -> PhysicalQueryT m a
                          -> m a
physicalQueryFromFunction pt func act =
  runReaderT (runPhysicalQueryT act) $ makePhysicalInfoHolder pt $ func pt

makeRandomParticle
  :: MonadRandom m
  => ParticleType -> m Particle
makeRandomParticle pt = do
  x <- getRandomR (0, pt ^. boxSizeX)
  y <- getRandomR (0, pt ^. boxSizeY)
  angle <- getRandomR (0, 2 * pi)
  return $
    Particle
      (P $ V2 x y)
      (angle ^. re angleIso)
      [Red, Blue, Red, Blue]

randomPhysicalQuery
  :: (MonadRandom m)
  => ParticleType -> PhysicalQueryT m a -> m a
randomPhysicalQuery pt act = do
  partsToAdd <- randomStarts pt $ fromIntegral $ pt ^. numParts
  physicalQueryFromFunction pt (const partsToAdd) act

randomStarts
  :: forall m.
     (MonadRandom m)
  => ParticleType -> Int -> m Frame
randomStarts pt n = I.fromList . zip [0 ..] <$> helper []
  where
    helper :: [Particle] -> m [Particle]
    helper xs
      | length xs == n = return xs
      | otherwise = do
        partToAdd <- makeRandomParticle pt
        newFrameEnergy <-
          physicalQueryFromFunction
            pt
            (const $ addParticleToFrame (I.fromList $ zip [0 ..] xs) partToAdd)
            (runPhysicalFold frameEnergy)
        if nearZero newFrameEnergy
          then helper (partToAdd : xs)
          else helper xs

withPlannedPhase
  :: (MonadIO m, MonadRandom m)
  => ParticleType -> PhaseType -> PhysicalQueryT m a -> m a
withPlannedPhase pt Kagome =
  physicalQueryFromFunction
    pt
    (\p ->
       I.fromList $ zip [0 ..] $
       kagomeGrid p (fromIntegral $ p ^. numParts))
withPlannedPhase pt Square =
  physicalQueryFromFunction
    pt
    (\p -> I.fromList $ zip [0 ..] $ squareGrid p s s)
  where
    s = floor $ sqrt $ fromIntegral $ pt ^. numParts
withPlannedPhase pt Rhombus =
  physicalQueryFromFunction
    pt
    (\p -> I.fromList $ zip [0 ..] $ diamondGrid p s s)
  where
    s = floor $ sqrt $ fromIntegral $ pt ^. numParts
withPlannedPhase _ Disordered = undefined -- const $ return $ Left "Unable to run disorederd at the moment"
withPlannedPhase pt Liquid = randomPhysicalQuery pt

makeSquareWall :: HasSimulationInfo s => s -> [Particle]
makeSquareWall pt =
    let helper n =
            Particle
            { _particlePos = P $ V2 0 $ fromIntegral n * pt ^. particleDiameter
            , _particleOrientation =
                  mkUnitVector $
                  V2
                      1
                      (if even n
                           then 1
                           else -1)
            , _particleColours = [Red, Blue, Red, Blue]
            }
    in map helper [0 .. floor (head $ pt ^.. boxSize)]

makeKagomeWall :: ParticleType -> [Particle]
makeKagomeWall pt =
    let [partA, partB] = set (traverse . pos . _x) 0 $
            ((kagGridSmall exp pt) ^.. itraversed . indices (`elem` [1, 2]))
        exp = 0
        helper n =
            (pos . _y .~ (1 + exp) * fromIntegral n * pt ^. particleDiameter)
                (if even n
                     then partA
                     else partB)
    in map (wrapPosition (pt ^. boxSizeX, pt ^. boxSizeY) . helper)
           [1 .. floor (pt ^. boxSizeY)]
