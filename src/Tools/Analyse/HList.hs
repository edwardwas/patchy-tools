{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}

module Tools.Analyse.HList where

import           Tools.Stats

import           Control.Monad.Identity
import           Data.Csv
import           Data.Semigroup
import qualified Data.Vector            as V
import           GHC.Exts               (Constraint)

type family AllConstraint (c :: * -> Constraint) xs :: Constraint where
  AllConstraint _ '[] = ()
  AllConstraint c (x : xs) = (c x, AllConstraint c xs)

type family AllConstraint2 (c :: * -> Constraint) f xs :: Constraint where
  AllConstraint2 _ f '[] = ()
  AllConstraint2 c f (x : xs) = (c (f x), AllConstraint2 c f xs)

type family Append xs ys where
  Append '[] ys = ys
  Append (x ': xs) ys = x ': (Append xs ys)

data HList f xs where
  EmptyHList :: HList f '[]
  HCons :: f a -> HList f as -> HList f (a ': as)

type HList' = forall f. HList f

extractValueHList
  :: (SumStat s, AllConstraint Fractional xs)
  => HList s xs -> HList Identity xs
extractValueHList EmptyHList = EmptyHList
extractValueHList (HCons a n) =
  HCons (Identity $ extractValue a) (extractValueHList n)

singleValueHList
  :: (SumStat s, AllConstraint Fractional xs)
  => HList Identity xs -> HList s xs
singleValueHList EmptyHList = EmptyHList
singleValueHList (HCons a n) =
  HCons (singleElement $ runIdentity a) (singleValueHList n)

(-:-)
  :: Applicative f
  => a -> HList f as -> HList f (a ': as)
(-:-) a = HCons (pure a)

infixr 5 -:-

(-:)
  :: Applicative f
  => a -> b -> HList f '[ a, b]
(-:) a b = a -:- b -:- EmptyHList

infixr 5 -:

combineHList :: HList f xs -> HList f ys -> HList f (Append xs ys)
combineHList EmptyHList x  = x
combineHList (HCons a n) b = HCons a (combineHList n b)

instance AllConstraint2 Show f xs =>
         Show (HList f xs) where
  show h = "HList f (" ++ showHelper h ++ ")"
    where
      showHelper
        :: (AllConstraint2 Show f xs)
        => HList f xs -> String
      showHelper EmptyHList = ""
      showHelper (HCons a (HCons b EmptyHList)) =
        show a ++ " -: " ++ show b
      showHelper (HCons a n) = show a ++ " -:- " ++ showHelper n

instance (AllConstraint2 Eq f xs) =>
         Eq (HList f xs) where
  EmptyHList == EmptyHList = True
  HCons a b == HCons c d = a == c && b == d

instance Ord (HList f '[]) where
  EmptyHList `compare` EmptyHList = EQ

instance ( Ord (f a)
         , Ord (HList f as)
         , Eq a
         , AllConstraint Eq as
         , AllConstraint2 Eq f as
         ) =>
         Ord (HList f (a ': as)) where
  HCons a b `compare` HCons c d = compare a c `mappend` compare b d

instance AllConstraint ToField as =>
         ToRecord (HList Identity as) where
  toRecord EmptyHList = V.empty
  toRecord (HCons a n) =
    V.singleton (toField $ runIdentity a) `mappend` toRecord n

instance Semigroup (HList f '[]) where
  EmptyHList <> EmptyHList = EmptyHList

instance Monoid (HList f '[]) where
  mempty = EmptyHList
  mappend _ _ = EmptyHList

instance (Semigroup (f a), Semigroup (HList f as)) =>
         Semigroup (HList f (a ': as)) where
    HCons a n <> HCons b m = HCons (a <> b) (n <> m)

instance ( Semigroup (f a)
         , Monoid (f a)
         , Monoid (HList f as)
         , Semigroup (HList f as)
         ) =>
         Monoid (HList f (a ': as)) where
    mempty = HCons mempty mempty
    mappend = (<>)
