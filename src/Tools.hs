{-# LANGUAGE OverloadedStrings #-}

module Tools
  ( module X
  , createContinue
  , loadContinue
  ) where

import           Tools.ConfigPrinter      as X
import           Tools.Draw               as X
import           Tools.FloodFill          as X
import           Tools.Graph              as X
import           Tools.P                  as X
import           Tools.Physical           as X
import           Tools.StartPositions     as X
import           Tools.Stats              as X
import           Tools.Template           as X
import           Tools.Types              as X
import           Tools.Util               as X

import           Control.Lens             as X hiding (each, (<~))
import           Control.Lens
import           Control.Monad.Reader     as X
import           Data.Aeson               as X hiding ((.=))
import           Data.Aeson.Encode.Pretty as X
import           Data.Foldable            (toList)
import           Data.Maybe               (fromJust)
import           Data.Monoid
import           Linear                   as X
import           Linear.Affine            as X
import           Pipes                    as X
import qualified Pipes.Prelude            as P
import           Pipes.Safe               as X

createContinue :: ParticleType -> Frame -> ParticleType
createContinue pt fr =
  pt & numParts .~ 0 & particlesToAdd .~ (toList fr) & fileName %~
  (\f -> "cont_" <> f)

loadContinue
  :: (MonadMask m, MonadIO m)
  => FilePath -> m ParticleType
loadContinue fp =
  runSafeT $ do
    pt <- loadParticleType fp
    lastFrame <- fromJust <$> P.last (frameProducer fp)
    return $ createContinue pt lastFrame
