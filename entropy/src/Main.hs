{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

import           Tools                       hiding (decode, encode, (<|))

import           Control.Concurrent          hiding (yield)
import           Control.Concurrent.Async    (mapConcurrently_)
import           Control.Monad.Par           (runPar)
import           Control.Monad.Par.Class
import           Control.Monad.Par.IO        (runParIO)
import           Control.Monad.Random
import           Control.Monad.State
import           Control.Monad.Trans.Control
import qualified Data.Aeson.Encode.Pretty    as A
import qualified Data.Array.Repa             as AR
import           Data.Bifunctor
import qualified Data.ByteString.Lazy        as B
import           Data.Csv
import qualified Data.DList                  as DL
import qualified Data.IntMap                 as I
import qualified Data.Map                    as M
import           Data.Monoid
import           GHC.Generics
import           GHC.IO.Handle
import           Pipes
import qualified Pipes.ByteString            as PB
import qualified Pipes.Csv                   as P
import qualified Pipes.Prelude               as P
import           Streamly
import qualified Streamly.Prelude            as S
import           System.IO
import           System.Random
import           Test.QuickCheck

data Move c = Move
  { moveTranslation :: !(V2 c)
  , moveRotation    :: !c
  , moveIndex       :: !Int
  } deriving (Functor, Eq, Show, Generic)

moveToTuple :: Move c -> (c,c,c,Int)
moveToTuple (Move (V2 x y) r i) = (x,y,r,i)

instance ToField c => ToRecord (Move c) where
  toRecord = toRecord . moveToTuple

makeRandomMove ::
     (Floating a, Random a, MonadRandom m) => a -> Int -> m (Move a)
makeRandomMove maxDelta maxSize = do
  let maxRotation = pi / 4
  dPos <-
    V2 <$> getRandomR (-maxDelta, maxDelta) <*>
    getRandomR (-maxDelta, maxDelta)
  dRad <- getRandomR (negate maxRotation, maxRotation)
  ind <- getRandomR (0, maxSize - 1)
  return $ Move dPos dRad ind

manyMoves ::
     ( MonadTrans t
     , IsStream t
     , MonadThrow m
     , MonadBaseControl IO m
     , MonadIO m
     , Floating a
     , Random a
     , Monad (t m)
     )
  => StdGen
  -> a
  -> Int
  -> t m (Move a)
manyMoves g maxDelta maxSize =
  let (move, g') = runRand (makeRandomMove maxDelta maxSize) g
  in return move `async` manyMoves g' maxDelta maxSize

applyMove :: Move Double -> Frame -> Frame
applyMove (Move tr rot ind) =
  (ix ind . pos . _Point +~ tr) .
  (ix ind . orientation . angleIso +~ rot)

moveIsSuccess :: Double -> ParticleType -> Frame -> Move Double -> Bool
moveIsSuccess oldEnergy pt parts m =
  let newEnergy =
        pureQueryFromFrame pt (applyMove m parts) $ runPhysicalFold frameEnergy
  in newEnergy <= oldEnergy

runSimProd ::
     (MonadRandom m)
  => Double
  -> ParticleType
  -> Frame
  -> Producer (Move Double) m a
runSimProd maxDelta pt fr =
  P.repeatM (makeRandomMove maxDelta (length fr)) >-> P.filter (moveIsSuccess oldEnergy pt fr)
  where
    oldEnergy = pureQueryFromFrame pt fr $ runPhysicalFold frameEnergy

streamSim ::
     ( MonadTrans t
     , IsStream t
     , MonadThrow m
     , MonadBaseControl IO m
     , Monad (t m)
     , MonadIO m
     )
  => StdGen
  -> Double
  -> ParticleType
  -> Frame
  -> t m (Move Double)
streamSim g maxDelta pt fr = S.filter (moveIsSuccess oldEnergy pt fr) $ manyMoves g maxDelta (length fr)
  where
    oldEnergy = pureQueryFromFrame pt fr $ runPhysicalFold frameEnergy

makeKagFrame :: ParticleType -> Frame
makeKagFrame pt = kagGridSmall (0.05 * pt ^. patchDiameter) pt

runSimStream ::
     (ParticleType -> Frame)
  -> Int
  -> Double
  -> ParticleType
  -> String
  -> StdGen
  -> Double
  -> IO ()
runSimStream frameFunc n maxDelta pt fileName g bondAngle =
  let pt' = pt & singleTheta .~ bondAngle
  in do putStrLn $ "Starting " ++ show bondAngle
        withFile fileName WriteMode $ \handle ->
          S.mapM_ (B.hPutStr handle . encode . pure) $
          parallely $ S.take n $ streamSim g maxDelta pt' (frameFunc pt')
        putStrLn $ "Finished " ++ show bondAngle

runSim ::
     (ParticleType -> Frame)
  -> Int
  -> Double
  -> ParticleType
  -> Handle
  -> StdGen
  -> Double
  -> IO ()
runSim frameFunc n maxDelta pt handle g bondAngle = do
  putStrLn $ "Starting " ++ show bondAngle
  let pt' = pt & (singleTheta .~ bondAngle)
  evalRandT
    (runEffect
       (runSimProd maxDelta pt' (frameFunc pt') >-> P.take n >-> P.encode >->
        PB.toHandle handle))
    g
  putStrLn $ "Done " ++ show bondAngle

-- | Generate n values evenly spaced between two numbers
manyBetween ::
     (Enum a, Ord a, Fractional a, Integral b) => b -> a -> a -> [a]
manyBetween n s l
  | s > l = manyBetween n l s
  | otherwise = [s,s + diff .. l]
  where
    diff = (l - s) / (fromIntegral (n - 1))

main = do
  let reps = 5 * 10 ^ 6
      delta = 0.15
      numRuns = 24
      angles = [0.6] --manyBetween numRuns 0.6 (pi / 4)
  putStrLn "Doing kag"
  mapConcurrently_
    (\ba ->
       newStdGen >>= \g ->
         withFile
           ("kagResult_" ++ show ba ++ ".csv")
           WriteMode
           (\h ->
              runSim
                makeKagFrame
                reps
                delta
                defaultParticleType
                h
                g
                ba))
    angles
  B.writeFile "kagSetupExtra.json" $
    A.encodePretty (defaultParticleType, makeKagFrame defaultParticleType)
  putStrLn "Doing square"
  mapConcurrently_
    (\ba ->
       newStdGen >>= \g ->
         withFile
           ("squareResult_" ++ show ba ++ ".csv")
           WriteMode
           (\h ->
              runSim
                (\pt -> I.fromList $ zip [0 ..] $ squareGrid pt 2 2)
                reps
                delta
                defaultParticleType
                h
                g
                ba))
    angles
  B.writeFile "squareSetup.json" $
    A.encodePretty (defaultParticleType, squareGrid defaultParticleType 2 2)
  putStrLn "Doing diamond"
  mapConcurrently_
    (\ba ->
       newStdGen >>= \g ->
         withFile
           ("diamonResult_" ++ show ba ++ ".csv")
           WriteMode
           (\h ->
              runSim
                (\pt -> I.fromList $ zip [0 ..] $ diamondGrid pt 2 2)
                reps
                delta
                defaultParticleType
                h
                g
                ba))
    angles
  B.writeFile "diamonSetup.json" $
    A.encodePretty (defaultParticleType, diamondGrid defaultParticleType 2 2)
