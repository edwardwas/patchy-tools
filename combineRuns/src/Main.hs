{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE Strict            #-}
{-# LANGUAGE TupleSections     #-}
{-# LANGUAGE TypeApplications  #-}
module Main where

import           Tools               hiding (Parser)

import           Data.Function
import           Data.List
import           Data.Maybe
import           Data.Monoid
import qualified Data.Text           as T
import           Data.Text.Lens
import           Options.Applicative
import qualified Pipes.Prelude       as P
import           System.Directory
import           System.FilePath
import           Text.Regex

getValues :: ParticleType -> (Double,Double,Int)
getValues pt = (pt ^. singleTheta, pt ^. sameEnergy, read runNumber)
  where
    Just (runNumber:_) =
      matchRegex (mkRegex "runNumber_([0-9]+)") (pt ^. fileName . from packed)

isExtended :: FilePath -> Bool
isExtended = isJust . matchRegex (mkRegex "_extended")

groupBySameRun :: MonadSafe f => [FilePath] -> f [[FilePath]]
groupBySameRun xs =
  map (map snd) .
  groupBy ((==) `on` getValues . fst) . sortBy (compare `on` getValues . fst) <$>
  mapM (\fp -> (, fp) <$> loadParticleType fp) xs

combineRuns :: FilePath -> FilePath -> FilePath -> IO ()
combineRuns oFile fa fb = do
   aContents <- readFile fa
   bContents <- drop 2 . dropWhile (/= '}') <$> readFile fb
   putStrLn $ "Done " ++ fa
   writeFile oFile $ aContents ++ bContents

data AppOptions = AppOptions
  { inputFiles :: [FilePath]
  , outputFile :: FilePath
  , renameFunc :: FilePath -> FilePath
  }

parseAppOptions :: Parser AppOptions
parseAppOptions =
  AppOptions <$> many (strArgument (metavar "INPUT")) <*>
  strOption (short 'o' <> long "output") <*>
  pure (\fp -> dropExtension fp ++ "_combined" ++ ".txt")

run :: AppOptions -> IO ()
run AppOptions {..} =
  let helper (a:b:_) = combineRuns (renameFunc nonExt) nonExt ext
        where
          (ext, nonExt) =
            if isExtended a
              then (a, b)
              else (b, a)
  in do putStrLn "Started"
        pairs <- runSafeT $ groupBySameRun inputFiles
        putStrLn "Pairs found"
        mapM_ helper pairs

main :: IO ()
main = execParser (info (helper <*> parseAppOptions) fullDesc ) >>= run
