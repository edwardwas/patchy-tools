{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ApplicativeDo #-}

import Tools hiding (elements)

import System.FilePath
import System.FilePath.Glob (glob)
import Test.DocTest

import Control.Applicative
import Control.Monad.IO.Class
import Control.Monad.Random
import Data.List
import Data.Maybe
import Data.Monoid
import Pipes.Safe
import System.Exit

main = return ()
