{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict              #-}

module Main where

import           Tools                    hiding (FirstFrame, LastFrame, Parser,
                                           argument, (.=))

import           Control.Concurrent       (threadDelay)
import           Control.Concurrent.Async
import           Control.Concurrent.STM
import qualified Data.ByteString.Lazy     as B
import           Data.Char
import           Data.Csv                 hiding (Parser)
import           Data.Maybe
import           Data.Monoid
import qualified Data.Vector              as V
import           Options.Applicative      hiding (header)
import           Pipes
import qualified Pipes.Prelude            as P
import           System.Directory

data FrameSelector
  = AllFrames
  | LastFrame
  | FirstFrame
  deriving (Eq, Show, Enum, Bounded)

data RunOptions = RunOptions
  { outputFile    :: FilePath
  , inputFiles    :: [FilePath]
  , frameSelector :: FrameSelector
  , jobs          :: Int
  , skipNewFiles  :: Bool
  } deriving (Eq, Show)

timeFromFrameNumber
  :: (Integral t, HasSimulationInfo pt)
  => pt -> t -> Double
timeFromFrameNumber pt n =
  let ns = fromIntegral $ pt ^. numSweeps
      sd = pt ^. spatialDelta
      pd = pt ^. particleDiameter
      t =
        if pt ^. exponentialSampling
          then 10 **
               (fromIntegral n * fromIntegral (pt ^. writeRate) *
                logBase 10 ns /
                ns)
          else fromIntegral n * fromIntegral (pt ^. writeRate)
  in t * sd * sd / (3 * pd * pd)

parseRunOptions :: Parser RunOptions
parseRunOptions =
  let readFrameSelector x
        | y == "all" = Just AllFrames
        | y == "last" = Just LastFrame
        | y == "first" = Just FirstFrame
        | otherwise = Nothing
        where
          y = map toLower x
  in RunOptions <$>
     option str (long "output" <> short 'o' <> value "data.csv") <*>
     many (argument str (metavar "INPUT")) <*>
     option
       (maybeReader readFrameSelector)
       (long "frame-selector" <> short 's' <> value AllFrames) <*>
     option auto (long "jobs" <> short 'j' <> value 1) <*>
     switch (long "skip-new-files")

selectedProducer
  :: forall m.
     MonadSafe m
  => RunOptions
  -> FrameSelector
  -> FilePath
  -> Producer (Double, Frame) m ()
selectedProducer RunOptions {..} AllFrames fp
  | skipNewFiles = do
    liftIO $ putStrLn $ "Skipping new files for " ++ fp
    pt <- lift $ loadParticleType fp
    frameProducer fp >-> indexPipe >->
      P.map (over _1 (timeFromFrameNumber pt))
  | otherwise = do
    pt <- lift $ loadParticleType fp
    let current :: Producer (Double, Frame) m () =
          frameProducer fp >-> indexPipe >->
          P.map (over _1 (timeFromFrameNumber pt))
    mLastTimeTuple <- lift $ P.last current
    case mLastTimeTuple of
      Nothing -> mempty
      Just (lastTime, _) -> do
        let newFileName = fp <> "_new"
        newFileExists <- liftIO $ doesFileExist newFileName
        let extra =
              if newFileExists
                then liftIO (putStrLn $ "Found " ++ newFileName) >>
                     selectedProducer
                       RunOptions {..}
                       AllFrames
                       newFileName
                else liftIO
                       (putStrLn $ "Could not find " ++ newFileName) >>
                     mempty
        current <> (extra >-> P.map (_1 +~ lastTime) >-> P.drop 1)
selectedProducer RunOptions {..} LastFrame fp =
  lift (P.last $ selectedProducer RunOptions {..} AllFrames fp) >>=
  yield . fromJust
selectedProducer RunOptions {..} FirstFrame fp =
  lift (P.head $ selectedProducer RunOptions {..} AllFrames fp) >>=
  yield . fromJust

getHeader :: NamedRecord -> Header
getHeader = V.fromList . toListOf (itraversed . asIndex)

makeQuery
  :: MonadPhysicalQuery m
  => Double -> m NamedRecord
makeQuery time = do
  numberOfParticles <- length <$> askParticles
  pt <- viewParticleType id
  (globalPhi6, localPhi6, globalPsi, localPsi, bonds, energy) <-
    runPhysicalFold
      ((,,,,,) <$> globalPhi6Query <*> localPhi6Query <*>
       globalPsiQuery <*>
       localPsiQuery <*>
       numberOfBonds <*>
       frameEnergy)
  simInfo <-
    (toNamedRecord . (numParts .~ fromIntegral numberOfParticles)) <$>
    viewParticleType simulationInfo
  return $
    [ "globalPhi6" .= globalPhi6
    , "localPhi6" .= localPhi6
    , "globalPsi" .= globalPsi
    , "localPsi" .= localPsi
    , "time" .= time
    , "energy" .= energy
    , "numberOfBonds" .=
      fromMaybe (-1 :: Double) (extractMeanMaybe bonds)
    ] `mappend`
    simInfo

splitListEqual :: Int -> [a] -> [[a]]
splitListEqual _ [] = []
splitListEqual n xs = take n xs : splitListEqual n (drop n xs)

handleSingleFile
  :: MonadSafe m
  => RunOptions
  -> TVar [NamedRecord]
  -> FrameSelector
  -> FilePath
  -> m ()
handleSingleFile RunOptions {..} q sel fp = do
  liftIO $ putStrLn $ "Doing " ++ fp
  pt <- loadParticleType fp
  runEffect $
    selectedProducer RunOptions {..} sel fp >->
    P.chain
      (\(n, _) ->
         liftIO $ putStrLn $ "Doing " ++ show n ++ " for " ++ fp) >->
    P.map (\(t, fr) -> makeQuery t $ makePhysicalInfoHolder pt fr) >->
    P.mapM_ (\x -> liftIO $ atomically $ modifyTVar' q (x :))
  liftIO $ putStrLn $ "Done " ++ fp

run :: RunOptions -> IO ()
run RunOptions {..} = do
  var :: TVar [NamedRecord] <- newTVarIO []
  let writeFile = do
        contents <- readTVarIO var
        unless (null contents) $
          B.writeFile outputFile $
          encodeByName (getHeader $ head contents) contents
  writeSync <- async $ forever $ writeFile >> threadDelay (10 ^ 7)
  mapM_
    (mapConcurrently
       (runSafeT .
        handleSingleFile RunOptions {..} var frameSelector)) $
    splitListEqual jobs inputFiles
  cancel writeSync
  writeFile

main :: IO ()
main =
  execParser (info (helper <*> parseRunOptions) fullDesc) >>= run
