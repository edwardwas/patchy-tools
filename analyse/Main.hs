module Main where

import Tools hiding ((.=), argument)
import Tools.Analyse

import System.Environment

main = do
  x:xs <- getArgs
  testAllFiles 12 [FrameNumber] [Energy] x xs
