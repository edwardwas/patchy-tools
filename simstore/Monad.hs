{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TupleSections #-}

module Monad where

import Types

import Tools
import Control.Monad.IO.Class
import Data.Digest.Pure.MD5
import System.FilePath
import Control.Monad.Reader
import Control.Concurrent.STM
import Control.Concurrent.Async
import Data.Acid
import System.Directory
import Data.Maybe
import Pipes.Csv
import System.IO
import qualified Pipes.ByteString as PB
import qualified Data.ByteString.Lazy as BL
import qualified Pipes.Prelude as P
import qualified Data.Binary as BIN

makeFrameNumber :: (Integral a, HasParticleType s) => s -> a -> Integer
makeFrameNumber pt n
  | pt^.exponentialSampling = floor $ exp $ a * fromIntegral n
  | otherwise = fromIntegral n * pt^.writeRate
    where a = log (fromIntegral (pt^.numSweeps)) / t
          t = fromIntegral $ pt^.numSweeps `div` pt^.writeRate

data Message = FilesEqualSkiping FilePath
        | ReplacementFile FilePath
        | NewFile FilePath
        | Running FilePath Int
        | Done FilePath
    deriving (Eq,Show)

class Monad m => MonadStore m where
    sendMessage :: Message -> m ()
    infoPipe :: Producer (ParticleType, Integer, StoreInfo) m ()
    fileHash :: FilePath -> m BL.ByteString
    storedHash :: FilePath -> m (Maybe BL.ByteString)
    captureFile :: FilePath -> m ()
    fileAlreadyProcessed :: FilePath -> m Bool
    createEmptyStoreObject :: FilePath -> m StoreObject
    createStoreInfo :: ParticleType -> Frame -> m StoreInfo
    insertNew :: FilePath -> StoreObject -> Producer (Integer, StoreInfo) m () -> m ()

writeInfo :: StoreM ()
writeInfo = do
    Just pth <- view (_3 . csvPath)
    handle <- liftIO $ openFile pth WriteMode
    runEffect $ infoPipe >-> encodeByName infoHeader >-> PB.toHandle handle
    liftIO $ hClose handle


handleFile :: (MonadStore m, MonadSafe m, MonadReader s m, Field3 s s UserInput UserInput) 
           => FilePath -> m ()
handleFile fp = do
    imagePrep <- view (_3 . imagePath)
    let imageFileName n = imagePrep </> (dropExtension (takeFileName fp) ++ "_sweep_" ++
            padNum 10 n ++ ".ps")
    fAlreadyDone <- fileAlreadyProcessed fp
    fFilesEqual <- (\a b -> Just a == b) <$> fileHash fp <*> storedHash fp
    case (fAlreadyDone,fFilesEqual) of
      (True,True) -> sendMessage $ FilesEqualSkiping fp
      (True,False) -> do
          sendMessage $ ReplacementFile fp
          captureFile fp
      (False,_) -> do
          sendMessage $ NewFile fp
          captureFile fp
    unless (fAlreadyDone && fFilesEqual) $ do
        so <- createEmptyStoreObject fp
        pt <- loadParticleType fp
        hs <- fileHash fp
        insertNew fp so $ frameProducer fp
            >-> indexPipe
            >-> P.chain (sendMessage . Running fp . fst)
            >-> P.chain (\(a,b) -> liftIO $ 
                drawToFile (imageFileName $ makeFrameNumber pt a) $
                    drawFrame pt b)
            >-> P.mapM (\(a,b) -> (makeFrameNumber pt a,) <$> createStoreInfo pt b)
        sendMessage $ Done fp

newtype StoreM a = StoreM (ReaderT (AcidState Storage, TChan Message, UserInput) (SafeT IO) a)
    deriving (Functor, Applicative, Monad, MonadIO, MonadCatch, MonadMask, MonadThrow,
             MonadReader (AcidState Storage, TChan Message, UserInput))

instance MonadSafe StoreM where
    type Base StoreM = IO
    liftBase = liftIO
    register = StoreM . register
    release = StoreM . release

runStoreM :: UserInput -> StoreM a -> IO a
runStoreM uInput (StoreM act) = do
    chan <- newTChanIO
    printingAsync <- async $ forever $ atomically (readTChan chan) >>= print
    state <- openLocalStateFrom (uInput ^. databaseStorePath) mempty 
    res <- runSafeT $ runReaderT act (state,chan,uInput) 
    closeAcidState state
    cancel printingAsync
    return res

runStoreMPar :: Foldable f => Int -> UserInput -> f (StoreM ()) -> IO ()
runStoreMPar n uInput acts = do
    chan <- newTChanIO
    printingAsync <- async $ forever $ atomically (readTChan chan) >>= print
    state <- openLocalStateFrom (uInput ^. databaseStorePath) mempty 
    workStealer_ n (\(StoreM act) -> runSafeT $ runReaderT act (state,chan,uInput)) $ each acts
    closeAcidState state
    cancel printingAsync



instance MonadStore StoreM where
    sendMessage m = StoreM $ do
        chan <- view _2
        liftIO $ atomically $ writeTChan chan m
    infoPipe = do
        state <- lift $ StoreM $ view _1
        allFiles <- liftIO $ query state RetrieveRecordedFiles
        each allFiles 
            >-> P.drop 1
            >-> P.mapM (\fp -> map (fp,) <$> liftIO (query state $ RetrieveRecordedInfoA fp)) 
            >-> P.concat
            >-> P.mapM (\(fp,n) -> fmap (fp,n,) <$> liftIO (query state $ RetrieveStoreInfoA fp n))
            >-> P.concat
            >-> P.mapM (\(fp,n,si) -> fmap (,n,si) <$> liftIO (query state $ RetrieveParticleTypeA fp))
            >-> P.concat
            >-> P.seq

    fileHash = liftIO . fmap (BIN.encode . md5) . BL.readFile 
    storedHash fp = do
        state <- StoreM $ view _1
        liftIO $ query state $ RetrieveStoreHash fp
         
    captureFile fp = do
        targetDir <- StoreM $ view (_3 . fileStorePath)
        liftIO $ copyFile fp $ targetDir </> takeFileName fp
    fileAlreadyProcessed fp = do
        state <- StoreM $ view _1
        isJust <$> liftIO (query state $ RetrieveParticleTypeA fp)
    createEmptyStoreObject fp = StoreObject
        <$> liftIO (runSafeT (loadParticleType fp))
        <*> pure mempty
        <*> fileHash fp
    createStoreInfo pt fr = liftIO $ physicalQueryFromFrame pt fr $ StoreInfo
        <$> localPsiQuery
        <*> localPhiQuery 6
        <*> globalPsiQuery
        <*> globalPhiQuery 6
        <*> fmap (fromMaybe 0 . extractMeanMaybe) numberOfBonds
        <*> frameEnergy
    insertNew fp so prod = do
        state <- StoreM $ view _1
        liftIO $ update state $ NewStoreObjectA fp (so^.storeParticleType) (so^.storeHashs)
        runEffect $ prod >-> P.mapM_ (\(n,s) -> liftIO $ update state $ NewStoreInfoA fp n s)



