{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Types where

import Tools hiding ((.=))
import Data.SafeCopy
import Data.Acid
import Data.Csv
import qualified Data.Map.Strict as M
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import qualified Data.Text as T
import qualified Data.Vector as V

data UserInput = UserInput {
    _databaseStorePath :: FilePath
  , _fileStorePath :: FilePath
  , _imagePath :: FilePath
  , _csvPath :: Maybe FilePath
  , _cores :: Int
  , _filesToHandle :: [FilePath]
                           } deriving (Eq,Show)
makeLenses ''UserInput

data StoreInfo = StoreInfo {
    _localPsi :: Double
  , _localPhi :: Double
  , _globalPsi :: Double
  , _globalPhi :: Double
  , _numBonds :: Double
  , _energy :: Double
                           } deriving (Eq,Show)
makeLenses ''StoreInfo
deriveSafeCopy 0 'base ''StoreInfo

data StoreObject = StoreObject {
    _storeParticleType :: ParticleType
  , _storeItems :: M.Map Integer StoreInfo
  , _storeHashs :: BL.ByteString
                               } deriving (Eq,Show)
makeLenses ''StoreObject
deriveSafeCopy 0 'base ''StoreObject

newtype Storage = Storage {_storage :: M.Map FilePath StoreObject}
    deriving (Eq,Show,Monoid)
makeLenses ''Storage
deriveSafeCopy 0 'base ''Storage

instance ToNamedRecord (ParticleType, Integer, StoreInfo) where
    toNamedRecord (pt,n,s) = fmap BC.pack [
        ("sameEnergy", show $ pt^.sameEnergy)
      , ("differentEnergy", show $ pt^.differentEnergy)
      , ("areaFraction", show $ pt^.areaFraction)
      , ("theta", show $ pt ^.singleTheta)
      , ("numParts", show $ pt^.numParts)
      , ("fileName", T.unpack $ pt^.fileName)
      , ("rotationalDelta", show $ pt^.rotationalDelta)
      , ("spatialDelta", show $ pt^.spatialDelta)
      , ("sweepNumber", show n)
      , ("localPsi", show $ s^.localPsi)
      , ("localPhi", show $ s^.localPhi)
      , ("globalPsi", show $ s^.globalPsi)
      , ("globalPhi", show $ s^.globalPhi)
      , ("numberOfBonds", show $ s^.numBonds)
      , ("energy", show $ s^.energy)
                                 ]

infoHeader :: V.Vector B.ByteString
infoHeader = ["sameEnergy", "differentEnergy", "areaFraction", "numParts", "fileName", "rotationalDelta", "spatialDelta", "sweepNumber", "localPsi", "localPhi", "globalPsi", "globalPhi", "numberOfBonds", "energy", "theta"] 

newStoreObjectA :: FilePath -> ParticleType -> BL.ByteString -> Update Storage ()
newStoreObjectA fp pt hs = storage . at fp ?= StoreObject pt M.empty hs

newStoreInfoA :: FilePath -> Integer -> StoreInfo -> Update Storage ()
newStoreInfoA fp n si = storage . at fp . traverse . storeItems %= M.insert n si

retrieveStoreInfoA :: FilePath -> Integer -> Query Storage (Maybe StoreInfo)
retrieveStoreInfoA fp n = preview $ storage . at fp . traverse . storeItems . at n . traverse

retrieveRecordedInfoA :: FilePath -> Query Storage [Integer]
retrieveRecordedInfoA fp = do
    so <- preview (storage . at fp . traverse . storeItems)  <$> ask
    case so of
      Nothing -> return []
      Just x -> return  $ M.keys x

retrieveRecordedFiles :: Query Storage [FilePath]
retrieveRecordedFiles = M.keys <$> view storage

retrieveParticleTypeA :: FilePath -> Query Storage (Maybe ParticleType)
retrieveParticleTypeA fp = preview (storage . at fp . traverse . storeParticleType)

retrieveStoreHash :: FilePath -> Query Storage (Maybe BL.ByteString)
retrieveStoreHash fp = preview (storage . at fp . traverse . storeHashs)

makeAcidic ''Storage ['newStoreObjectA, 'newStoreInfoA, 'retrieveStoreInfoA, 
        'retrieveRecordedInfoA, 'retrieveRecordedFiles, 'retrieveParticleTypeA,
                     'retrieveStoreHash]
