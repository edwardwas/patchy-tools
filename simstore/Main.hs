import Types
import Monad

import Options.Applicative
import Control.Lens hiding (argument)
import Data.Maybe
import Control.Monad
import System.Directory
import System.FilePath

runWithOptions ui = do
    print ui
    runStoreMPar (ui^.cores) ui $ map handleFile $ ui^.filesToHandle
    when (isJust $ ui^.csvPath) $ runStoreM ui writeInfo

main = do
    homeDir <- getHomeDirectory
    let opts = info parser mempty
        parser = UserInput
            <$> strOption (metavar "DATABASE" <> short 'd' <> long "database" <> value (homeDir </> ".simstore" </> "database"))
            <*> strOption (metavar "FILESTORE" <> short 'f' <> long "file-store" <> value (homeDir </> ".simstore" </> "fileStore"))
            <*> strOption (metavar "IMAGESTORE" <> short 'i' <> long "image-store" <> value (homeDir </> ".simstore" </> "imageStore"))
            <*> optional (strOption $ metavar "CSV" <> short 'c' <> long "csv-out")
            <*> option auto (metavar "CORES" <> short 'n' <> long "corese" <> value 24)
            <*> many (strArgument $ metavar "FILES")
    execParser opts >>= runWithOptions  

