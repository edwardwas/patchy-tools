{compiler ? "default"}:
let
  pkgs = import ./common.nix {inherit compiler;};
in
  {
    tools = pkgs.haskellPackages.tools;
    videoMaker = pkgs.haskellPackages.videoMaker;
    combineRuns = pkgs.haskellPackages.combineRuns;
    entropy = pkgs.haskellPackages.entropy;
    makePlotting = pkgs.haskellPackages.makePlotting;
  }
