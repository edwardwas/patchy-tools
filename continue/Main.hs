{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE Strict #-}

module Main where

import Tools

import Control.Applicative
import Data.Aeson.Encode.Pretty
import qualified Data.ByteString.Lazy as B
import Data.Foldable (toList)
import Data.Maybe
import Data.Monoid
import Data.Text.Lens
import Options.Applicative
import qualified Pipes.Prelude as P
import System.FilePath
import System.Random

data AppOptions = AppOptions
  { inputFiles :: [FilePath]
  , outputFile :: FilePath
  , jobs :: Int
  } deriving (Eq, Show)

parseAppOptions :: Parser AppOptions
parseAppOptions =
  AppOptions <$> many (strArgument (metavar "INPUT")) <*>
  strOption (metavar "OUTPUT" <> short 'o' <> long "out") <*>
  option auto (metavar "N" <> short 'j' <> long "jobs" <> value 24)

createCompleterRun ::
     MonadSafe m => FilePath -> m (Maybe ParticleType)
createCompleterRun fp = do
  newSeed <- abs <$> liftIO randomIO
  pt <- loadParticleType fp
  let numFramesRequired =
        fromIntegral $ pt ^. numSweeps `div` pt ^. writeRate
      wr = pt ^. writeRate
  numCompleted <- (\x -> x - 2) <$> P.length (frameProducer fp)
  liftIO $ putStrLn $ fp ++ ": " ++ show numCompleted
  if numCompleted >= numFramesRequired
    then return Nothing
    else do
      Just lastFrame <- P.last $ frameProducer fp
      return $ Just $ pt & (particlesToAdd .~ toList lastFrame) &
        (fileName . unpacked %~
         (\x -> dropExtension x ++ "_extended.txt")) &
        (simulationSeed %~ (<|> Just newSeed)) &
        (numSweeps .~ wr *
         fromIntegral (numFramesRequired - numCompleted))

runAppOptions :: AppOptions -> IO ()
runAppOptions AppOptions {..} =
  runSafeT
    (P.fold
       (flip (:))
       []
       catMaybes
       (workStealer jobs createCompleterRun $ each inputFiles)) >>=
  B.writeFile outputFile .
  encodePretty

main =
  execParser (info (helper <*> parseAppOptions) fullDesc) >>=
  runAppOptions
