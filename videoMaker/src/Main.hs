{-# LANGUAGE AllowAmbiguousTypes        #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE Strict                     #-}
{-# LANGUAGE TypeFamilies               #-}

module Main where

import           Tools                       hiding (FirstFrame, LastFrame,
                                              Parser, dot, has, view)

import           Data.Char
import           Data.Monoid
import           Data.Semigroup              hiding (option, (<>))
import qualified Data.Set                    as S
import qualified Data.Text                   as T
import           Diagrams                    (dims, renderDia)
import           Diagrams.Backend.Postscript
import           Diagrams.Backend.Rasterific
import           Options.Applicative
import           Pipes
import qualified Pipes.Prelude               as P
import qualified Shelly                      as Sh
import           System.Directory
import           System.FilePath

data FrameSelectionElement
  = AllFrames
  | LastFrame
  | FirstFrame
  deriving (Eq, Show, Ord)

newtype FrameSelection =
  FrameSelection (S.Set FrameSelectionElement)
  deriving (Eq, Show, Monoid, Semigroup)

usesAllFrames :: FrameSelection -> Bool
usesAllFrames (FrameSelection s) = AllFrames `S.member` s

data AppOptions = AppOptions
  { frameSelection     :: FrameSelection
  , numberOfJobs       :: Int
  , inputFiles         :: [FilePath]
  , outputDir          :: FilePath
  , shouldCreateVideo  :: Bool
  , tryAndFindNewFiles :: Bool
  , displayAppOptions  :: Bool
  , makePostScript     :: Bool
  } deriving (Eq, Show)

parseAppOptions :: Parser AppOptions
parseAppOptions =
  AppOptions <$>
  fmap
    mconcat
    (many $
     option
       (eitherReader readFrame)
       (short 'f' <> long "frame" <> metavar "FRAME-SELECTOR" <>
        help "Which frames to create pictures of. Multiple can be selected")) <*>
  option
    auto
    (short 'j' <> long "jobs" <> value 24 <> metavar "N" <>
     help "How many jobs to use") <*>
  many (strArgument (metavar "INPUT" <> help "Input files")) <*>
  strOption
    (short 'o' <> long "output" <> value "pics" <> metavar "OUTPUT" <>
     help "Output directory") <*>
  switch
    (short 'v' <> long "make-video" <>
     help "Should we create a video of the pictures") <*>
  (not <$>
   switch
     (long "skip-new-files" <>
      help "Do not look for files which extend the simulation")) <*>
  switch (long "display-options") <*>
  switch (long "make-postscript" <> help "Produce postscript files")
  where
    readFrame x =
      case map toLower x of
        "all"   -> Right $ FrameSelection $ S.singleton AllFrames
        "last"  -> Right $ FrameSelection $ S.singleton LastFrame
        "first" -> Right $ FrameSelection $ S.singleton FirstFrame
        _       -> Left "The valid options for frames are all, last or first"

selectedProducer ::
     Monad m => FrameSelection -> Producer a m () -> Producer a m ()
selectedProducer (FrameSelection s) p =
  if AllFrames `S.member` s
    then p
    else do
      end <-
        if LastFrame `S.member` s
          then lift $ P.last p
          else return Nothing
      start <-
        if FirstFrame `S.member` s
          then lift $ P.head p
          else return Nothing
      each [start, end] >-> P.concat

interiorCores :: AppOptions -> Int
interiorCores AppOptions {..} =
  if usesAllFrames frameSelection
    then numberOfJobs
    else 1

exteriorCores :: AppOptions -> Int
exteriorCores AppOptions {..} =
  if usesAllFrames frameSelection
    then 1
    else numberOfJobs

makeProducer ::
     forall m. MonadSafe m
  => AppOptions
  -> FilePath
  -> m (Producer (Int, Frame) m ())
makeProducer AppOptions {..} fp
  | not tryAndFindNewFiles = return (frameProducer fp >-> indexPipe)
  | otherwise = do
    let current = frameProducer fp >-> indexPipe
    posLast <- P.last current
    case posLast of
      Just (maxCount, _) -> do
        let newFileName = fp <> "_new"
        nextFileExists <- liftIO $ doesFileExist newFileName
        extra <-
          if nextFileExists
            then liftIO (putStrLn ("Found " ++ newFileName)) >>
                 makeProducer AppOptions {..} newFileName
            else liftIO (putStrLn ("Did not find " ++ newFileName)) >>
                 return mempty
        return $ current <> (extra >-> P.map (_1 +~ maxCount + 1))
      Nothing -> liftIO (putStrLn "No match") >> return mempty

runSingle :: (MonadSafe m, ParIO m) => AppOptions -> String -> m ()
runSingle ao@AppOptions {..} fp =
  let makeFileName n =
        outputDir </>
        (dropExtension fp ++
         "_image_" ++
         padNum 4 n ++
         if makePostScript
           then ".ps"
           else ".png")
      helper pt n fr =
        liftIO $ do
          putStrLn $ "Doing file " <> fp <> ": Frame " <> show n
          drawFunc n pt fr
      videoName = outputDir </> (dropExtension fp ++ ".mp4")
      drawFunc n pt fr =
        if makePostScript
          then renderDia
                 Postscript
                 (PostscriptOptions (makeFileName n) (dims $ pure 600) EPS) $
               drawFrame pt fr
          else renderRasterific (makeFileName n) (dims $ pure 600) $
               drawFrame pt fr
  in do prod <-
          selectedProducer frameSelection <$> makeProducer AppOptions {..} fp
        pt <- loadParticleType fp
        numFrames <- P.length prod
        videoExists <- liftIO $ doesPathExist videoName
        when (numFrames > 1) $
          liftIO $ putStrLn $ "Doing " ++ show numFrames ++ " frames"
        workStealer_ (interiorCores ao) (uncurry (helper pt)) prod
        when shouldCreateVideo $ do
          when videoExists $ liftIO $ removeFile videoName
          liftIO $ putStrLn $ "Making video " ++ videoName
          void $
            Sh.shelly $
            Sh.silently $
            Sh.run
              "ffmpeg"
              [ "-framerate"
              , "2"
              , "-i"
              , T.pack (outputDir </> (dropExtension fp ++ "_image_%04d.png"))
              , "-pix_fmt"
              , "yuv420p"
              , "-vcodec"
              , "libx264"
              , "-s"
              , "600x600"
              , T.pack videoName
              ]

runAppOptions :: AppOptions -> IO ()
runAppOptions ao = do
  createDirectoryIfMissing True $ outputDir ao
  runSafeT $
    workStealer_ (exteriorCores ao) (runSingle ao) $ each $ inputFiles ao

main :: IO ()
main = do
  options <- execParser (info (helper <*> parseAppOptions) fullDesc)
  when (displayAppOptions options) $ print options
  runAppOptions options
