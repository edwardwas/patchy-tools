{compiler ? "default"}:
let
  addBuildTools = dir : p: p.overrideAttrs (old: rec {
    nativeBuildInputs = old.nativeBuildInputs ++
      [ pkgs.haskellPackages.cabal-install
        pkgs.haskellPackages.ghcid
      ];
    shellHook = old.shellHook + "\ncd " + dir;
  });
  pkgs = import ./common.nix {inherit compiler;};
  tools = addBuildTools "." (pkgs.haskellPackages.tools.env);
  videoMaker = addBuildTools "videoMaker" (pkgs.haskellPackages.videoMaker.env);
  combineRuns = addBuildTools "combineRuns" (pkgs.haskellPackages.combineRuns.env);
  entropy = addBuildTools "entropy" (pkgs.haskellPackages.entropy.env);
  makePlotting = addBuildTools "makePlotting" (pkgs.haskellPackages.makePlotting.env);
in
  {
    inherit tools videoMaker combineRuns entropy makePlotting;
  }
